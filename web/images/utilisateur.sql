-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : lun. 22 mars 2021 à 15:24
-- Version du serveur :  5.7.33-cll-lve
-- Version de PHP : 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `saladile_saladile`
--

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` int(11) NOT NULL,
  `level_id` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `organisation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pays` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fond` double DEFAULT NULL,
  `github_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `github_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleplus_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleplus_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stackexchange_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stackexchange_access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `level_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `nom`, `ville`, `organisation`, `pays`, `telephone`, `adresse`, `fond`, `github_id`, `github_access_token`, `facebook_id`, `facebook_access_token`, `googleplus_id`, `googleplus_access_token`, `stackexchange_id`, `stackexchange_access_token`, `date`) VALUES
(1, 1, 'saladile', 'saladile', 'admin@saladile.fr', 'admin@saladile.fr', 1, NULL, '$2y$13$PD6SbPcV7EfBJsLatKEwOO8vEgV67IW2iHS1AtBSWSvC6rkyO7YyC', '2021-03-16 10:37:54', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', 'Admin Saladile', 'Nantes', 'NADERIC /SALAD\'ILE', 'France', '(+033) 681-09-12-30', '6, Rue Grande Biesse 44 200 Nantes', 27.6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 3, 'ERICBOR', 'ericbor', 'ericbordeau85@gmail.com', 'ericbordeau85@gmail.com', 0, NULL, '$2y$13$MaNagijopMb0TTgNUVwSS.G7NKmmI88Ow2lwskfv66izbYKCujLuG', '2020-12-16 19:07:32', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'BORDEAU', NULL, NULL, 'France', '(+033) 783-47-13-13', NULL, 500, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 3, 'nadbor', 'nadbor', 'ericbordeau@sfr.fr', 'ericbordeau@sfr.fr', 0, NULL, '$2y$13$FlpZkkRoneHUm2v1lmKRxu0/bqRP9WOf4a.7gdAf5LWF2I5DL0WVK', NULL, 'N972liHA9MIdJUc_FGI8l9Ilx6Yb2p41t7r34YnhVZ8', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'BORDEAU', 'challans', NULL, 'France', '(+033) 681-09-12-30', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 3, 'louisbordeau85@gmail.com', 'louisbordeau85@gmail.com', 'louisbordeau85@gmail.com', 'louisbordeau85@gmail.com', 1, NULL, '$2y$13$9d7ab9Iagz6A9jz0UA1wp.THml4PuOpygMIFnIOB18v4pg3GsWXqC', '2021-02-20 23:16:19', 'H5g2p2uYIDbwBtHMPL3W6edmm7z98cIth4uZ4asd0a8', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'BORDEAU', NULL, NULL, NULL, '(+033) 783-47-13-13', NULL, 17.3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2002-06-20'),
(37, 3, 'c.savary@batinantes.fr', 'c.savary@batinantes.fr', 'c.savary@batinantes.fr', 'c.savary@batinantes.fr', 1, NULL, '$2y$13$ly4K6du1pJcvu.lXmahUke1.APP2NKeG3MjjMTDcRf59WBSttqROG', '2021-03-22 10:46:09', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'SAVARY', NULL, NULL, 'France', '(+033) 632-34-88-56', NULL, 37.2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 3, 'aurelie.poupon@gmail.com', 'aurelie.poupon@gmail.com', 'aurelie.poupon@gmail.com', 'aurelie.poupon@gmail.com', 1, NULL, '$2y$13$jAzs27ziLFxRjn8LDeoSXuzPs1AzTvSFBeoqmxmaEJMV.sy.Aycwu', '2020-12-16 13:06:33', 'IWIPzdYKhcdHKDRrnAm_512o-ue3C_dRO4yBXG5KHis', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Poupon', NULL, NULL, NULL, '0650334964', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-16'),
(39, 3, 'maryanne.audran@gmail.com', 'maryanne.audran@gmail.com', 'maryanne.audran@gmail.com', 'maryanne.audran@gmail.com', 1, NULL, '$2y$13$F3iMLoqJ0ByTFYQvBzfnv.udsOHfLNIiTKyEIqQffru4I21ZMOVre', '2021-03-22 10:41:49', 'CUmVu3XwCOjLGclJm75Zg6VReFmf0HJ1un4X8zB59Tg', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Maryanne AUDRAN', NULL, NULL, NULL, '(+033) 640-21-10-97', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1995-06-13'),
(40, 3, 'myl_1505@hotmail.com', 'myl_1505@hotmail.com', 'myl_1505@hotmail.com', 'myl_1505@hotmail.com', 1, NULL, '$2y$13$axr4CTekdsj0VfiXhbNWE.4nRy5BXxYvL1.2n75w5ITMEAPbOeLdC', '2021-03-22 11:02:19', 'Exbphcmb8p5AnIErYBVox1xW9np8LwQ3_etsL7xUfWc', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'SCHNEIDER', NULL, NULL, NULL, '(+033) 683-00-04-77', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1983-05-15'),
(41, 3, 'ubuntu', 'ubuntu', 'juniorubuntu54@gmail.com', 'juniorubuntu54@gmail.com', 1, NULL, '$2y$13$pC9AKS4jeaUBSInG38hpi.YKcdoG.4yruzN2geumqzag34TnVS4xa', '2021-03-05 10:04:06', '8SCOOc11o-2luiXtvYAJIo62ka1vGM6e21J-QShPYio', '2021-02-02 10:45:21', 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Junior Ubuntu', 'Yaoundé', 'GST', 'France', '(+033) 688-79-70-76', '237 Rue Melen', 38.5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 3, 'Sarahmalary@yahoo.fr', 'sarahmalary@yahoo.fr', 'Sarahmalary@yahoo.fr', 'sarahmalary@yahoo.fr', 1, NULL, '$2y$13$qASByI4oLzj0ocTeBx2WkOZdMcVp1sp9yCFqKH38DZHCrTs7JW5WG', NULL, 'BXbfA-CVnWDeOVPDc45jTWbyWK-rdRVMR5UrUwGVXog', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Sarah MALARY', NULL, NULL, NULL, '(+033) 613-58-03-17', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1988-08-28'),
(43, 3, 'esseautania@gmail.com', 'esseautania@gmail.com', 'esseautania@gmail.com', 'esseautania@gmail.com', 1, NULL, '$2y$13$PPyIEHzkEg5UQPWqDGdR6OpUqKOjoAyOHT.gunJ.qzBghDNwvXQYS', '2021-02-18 08:38:35', 'tI2YS6Rc2hJWI43zeJAvFoNdhYG0RK-R-55dagE2kv8', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Tania Esseau', NULL, NULL, NULL, '0620712201', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1996-08-07'),
(44, 3, 'gemmelasalade@trashinbox.net', 'gemmelasalade@trashinbox.net', 'gemmelasalade@trashinbox.net', 'gemmelasalade@trashinbox.net', 1, NULL, '$2y$13$ZSsn.J9mUTbslvU0oaTaNO/PLef5BzXc62itg6.r5HucpYpQG2gSq', '2021-03-03 11:39:54', 'xl-moXN-_hSEFNFQrdNP8z_VRuRi_n9hleaBj6xUzRQ', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'DOUCET', NULL, NULL, NULL, '(+033) 010-20-30-40', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-03'),
(45, 3, 'jmarais@previa.fr', 'jmarais@previa.fr', 'jmarais@previa.fr', 'jmarais@previa.fr', 1, NULL, '$2y$13$zDo15JEVXzlV70XsFskeDucaVdXofFGx9pqZMeEOycWF73KVhjvea', '2021-03-22 10:48:29', 'oNaNix5VojC05afX9mPYsLc1VZb5RpfBGKtl7KCvpJc', NULL, 'a:1:{i:0;s:11:\"ROLE_CLIENT\";}', 'Joana MARAIS', NULL, NULL, NULL, '(+033) 659-41-47-33', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1992-10-03');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_1D1C63B392FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_1D1C63B3A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_1D1C63B3C05FB297` (`confirmation_token`),
  ADD KEY `IDX_1D1C63B35FB14BA7` (`level_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `FK_1D1C63B35FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `droit` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
