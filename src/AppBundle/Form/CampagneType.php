<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class CampagneType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')
        ->add('description')
        ->add('typeCampagne', ChoiceType::class, array(
            'choices'  => array(
                'Collecte de fonds' => 'COLLECTE',
                'Adhésion membres' => 'ADHESION',
            )))
        ->add('payante')
        ->add('montant', null, [
                'attr' => [
                    'type' => 'number'
                ]
            ]
        )
        ->add('illimite')
        ->add('dateDebut', DateType::class, array(
            'input'  => 'datetime',
            'widget' => 'single_text',
        ))
        ->add('dateFin', DateType::class, array(
            'input'  => 'datetime',
            'widget' => 'single_text',
        ))
        ->add('fermer')
        ->add('application', null, ['attr' => ['required' => 'required']])->add('categorie')
        ->add('image', FileType::class, ['required' => false]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Campagne'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_campagne';
    }


}
