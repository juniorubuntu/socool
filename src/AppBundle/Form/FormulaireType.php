<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormulaireType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('listeChamps', \Symfony\Component\Form\Extension\Core\Type\ChoiceType::class, array(
                    'choices' => array(
                        'Noms et prénoms' => 'Noms et prénoms', 
                        'Montant' => 'Montant', 
                        'Téléphone' => 'Téléphone',
                        'Profession' => 'Profession',
                        'Pays' => 'Pays',
                        'Ville' => 'Ville',
                        'Type de Membre' => 'Type de Membre',
                        'Age' => 'Age',
                        'Motivation' => 'Motivation',
                    ),
                    'expanded' => true,
                    'multiple' => true
                ))
        ->add('campagne');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Formulaire'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_formulaire';
    }


}
