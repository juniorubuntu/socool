<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AdhesionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom')->add('email')->add('telephone')->add('dateAdhesion')->add('typeMembre', ChoiceType::class, array(
            'choices'  => array(
                'Type de membre' => '',
                'Membre simple' => 'Membre simple',
                'Volontaire' => 'Volontaire',
                'Bénévole' => 'Bénévole',
            )))
        ->add('profession')->add('pays')->add('ville')->add('age')->add('motivation')->add('campagne')
        ->add('sexe', ChoiceType::class, array(
            'choices'  => array(
                'Civilité' => '',
                'Homme' => 'Homme',
                'Femme' => 'Femme',
            )))
        ->add('application');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Adhesion'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_adhesion';
    }


}
