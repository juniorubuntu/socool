<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * @ORM\Table(name="Campagne")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CampagneRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Campagne
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->getFermer(FALSE);
        $this->setDateCreation(new \DateTime());
    }

    public function __toString(){
        return $this->getNom();
    }

    use Timestamps;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=1000, nullable=false)
     */
    private $nom;



    /**
     * @var string
     *
     * @ORM\Column(name="typeCampagne", type="string", nullable=true)
     */
    private $typeCampagne;



    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur")
     */
    private $utilisateur;
    

    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application")
     */
    private $application;



    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categorie")
     */
    private $categorie;


    /**
     * @var bool
     *
     * @ORM\Column(name="illimite", type="boolean", nullable=true)
     */
    private $illimite;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDebut", type="datetime", nullable=true)
     */
    private $dateDebut;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateFin", type="datetime", nullable=true)
     */
    private $dateFin;



    /**
     * @var bool
     *
     * @ORM\Column(name="fermer", type="boolean", nullable=true)
     */
    private $fermer;


    /**
     * @var bool
     *
     * @ORM\Column(name="payante", type="boolean", nullable=true)
     */
    private $payante;


    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", nullable=true)
     */
    private $montant;


    /**
     * @var bool
     *
     * @ORM\Column(name="paye", type="boolean", nullable=true)
     */
    private $paye;


    /**
     * @var string
     *
     * @ORM\Column(name="session", type="string", nullable=true)
     */
    private $session;




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreation", type="datetime", nullable=true)
     */
    private $dateCreation;


    /**
     * @var object
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Collecte", mappedBy="campagne")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $collectes;


    /**
     * @var object
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Adhesion", mappedBy="campagne")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $adhesions;

    /**
     * @var object
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Formulaire", mappedBy="campagne")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $formulaires;


    /**
     * @var object
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Versement", mappedBy="versement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $versements;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Campagne
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Campagne
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set limite
     *
     * @param boolean $limite
     *
     * @return Campagne
     */
    public function setLimite($limite)
    {
        $this->limite = $limite;

        return $this;
    }

    /**
     * Get limite
     *
     * @return boolean
     */
    public function getLimite()
    {
        return $this->limite;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Campagne
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Campagne
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set fermer
     *
     * @param boolean $fermer
     *
     * @return Campagne
     */
    public function setFermer($fermer)
    {
        $this->fermer = $fermer;

        return $this;
    }

    /**
     * Get fermer
     *
     * @return boolean
     */
    public function getFermer()
    {
        return $this->fermer;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     *
     * @return Campagne
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\Utilisateur $utilisateur
     *
     * @return Campagne
     */
    public function setUtilisateur(\UserBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set application
     *
     * @param \AppBundle\Entity\Application $application
     *
     * @return Campagne
     */
    public function setApplication(\AppBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \AppBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\Categorie $categorie
     *
     * @return Campagne
     */
    public function setCategorie(\AppBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set illimite
     *
     * @param boolean $illimite
     *
     * @return Campagne
     */
    public function setIllimite($illimite)
    {
        $this->illimite = $illimite;

        return $this;
    }

    /**
     * Get illimite
     *
     * @return boolean
     */
    public function getIllimite()
    {
        return $this->illimite;
    }

    /**
     * Add collecte
     *
     * @param \AppBundle\Entity\Collecte $collecte
     *
     * @return Campagne
     */
    public function addCollecte(\AppBundle\Entity\Collecte $collecte)
    {
        $this->collectes[] = $collecte;

        return $this;
    }

    /**
     * Remove collecte
     *
     * @param \AppBundle\Entity\Collecte $collecte
     */
    public function removeCollecte(\AppBundle\Entity\Collecte $collecte)
    {
        $this->collectes->removeElement($collecte);
    }

    /**
     * Get collectes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCollectes()
    {
        return $this->collectes;
    }

    /**
     * Add adhesion
     *
     * @param \AppBundle\Entity\Adhesion $adhesion
     *
     * @return Campagne
     */
    public function addAdhesion(\AppBundle\Entity\Adhesion $adhesion)
    {
        $this->adhesions[] = $adhesion;

        return $this;
    }

    /**
     * Remove adhesion
     *
     * @param \AppBundle\Entity\Adhesion $adhesion
     */
    public function removeAdhesion(\AppBundle\Entity\Adhesion $adhesion)
    {
        $this->adhesions->removeElement($adhesion);
    }

    /**
     * Get adhesions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdhesions()
    {
        return $this->adhesions;
    }

    /**
     * Set typeCampagne
     *
     * @param string $typeCampagne
     *
     * @return Campagne
     */
    public function setTypeCampagne($typeCampagne)
    {
        $this->typeCampagne = $typeCampagne;

        return $this;
    }

    /**
     * Get typeCampagne
     *
     * @return string
     */
    public function getTypeCampagne()
    {
        return $this->typeCampagne;
    }

    /**
     * Add formulaire
     *
     * @param \AppBundle\Entity\Formulaire $formulaire
     *
     * @return Campagne
     */
    public function addFormulaire(\AppBundle\Entity\Formulaire $formulaire)
    {
        $this->formulaires[] = $formulaire;

        return $this;
    }

    /**
     * Remove formulaire
     *
     * @param \AppBundle\Entity\Formulaire $formulaire
     */
    public function removeFormulaire(\AppBundle\Entity\Formulaire $formulaire)
    {
        $this->formulaires->removeElement($formulaire);
    }

    /**
     * Get formulaires
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFormulaires()
    {
        return $this->formulaires;
    }

    /**
     * Set payante
     *
     * @param boolean $payante
     *
     * @return Campagne
     */
    public function setPayante($payante)
    {
        $this->payante = $payante;

        return $this;
    }

    /**
     * Get payante
     *
     * @return boolean
     */
    public function getPayante()
    {
        return $this->payante;
    }

    /**
     * Set paye
     *
     * @param boolean $paye
     *
     * @return Campagne
     */
    public function setPaye($paye)
    {
        $this->paye = $paye;

        return $this;
    }

    /**
     * Get paye
     *
     * @return boolean
     */
    public function getPaye()
    {
        return $this->paye;
    }

    /**
     * Set session
     *
     * @param string $session
     *
     * @return Campagne
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Campagne
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Campagne
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Add versement
     *
     * @param \AppBundle\Entity\Versement $versement
     *
     * @return Campagne
     */
    public function addVersement(\AppBundle\Entity\Versement $versement)
    {
        $this->versements[] = $versement;

        return $this;
    }

    /**
     * Remove versement
     *
     * @param \AppBundle\Entity\Versement $versement
     */
    public function removeVersement(\AppBundle\Entity\Versement $versement)
    {
        $this->versements->removeElement($versement);
    }

    /**
     * Get versements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVersements()
    {
        return $this->versements;
    }
}
