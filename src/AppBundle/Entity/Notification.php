<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NotificationRepository")
 */
class Notification
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255, nullable=true)
     */
    private $reference;


    /**
     * @var string
     *
     * @ORM\Column(name="sujetclient", type="string", length=255, nullable=true)
     */
    private $sujetClient;

    /**
     * @var string
     *
     * @ORM\Column(name="contenuclient", type="text", nullable=true)
     */
    private $contenuClient;

    /**
     * @var string
     *
     * @ORM\Column(name="sujetadmin", type="string", length=255, nullable=true)
     */
    private $sujetAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="contenuadmin", type="text", nullable=true)
     */
    private $contenuAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="variables", type="text", nullable=false)
     */
    private $variables;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     *
     * @return Notification
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set sujetClient
     *
     * @param string $sujetClient
     *
     * @return Notification
     */
    public function setSujetClient($sujetClient)
    {
        $this->sujetClient = $sujetClient;

        return $this;
    }

    /**
     * Get sujetClient
     *
     * @return string
     */
    public function getSujetClient()
    {
        return $this->sujetClient;
    }

    /**
     * Set contenuClient
     *
     * @param string $contenuClient
     *
     * @return Notification
     */
    public function setContenuClient($contenuClient)
    {
        $this->contenuClient = $contenuClient;

        return $this;
    }

    /**
     * Get contenuClient
     *
     * @return string
     */
    public function getContenuClient()
    {
        return $this->contenuClient;
    }

    /**
     * Set sujetAdmin
     *
     * @param string $sujetAdmin
     *
     * @return Notification
     */
    public function setSujetAdmin($sujetAdmin)
    {
        $this->sujetAdmin = $sujetAdmin;

        return $this;
    }

    /**
     * Get sujetAdmin
     *
     * @return string
     */
    public function getSujetAdmin()
    {
        return $this->sujetAdmin;
    }

    /**
     * Set contenuAdmin
     *
     * @param string $contenuAdmin
     *
     * @return Notification
     */
    public function setContenuAdmin($contenuAdmin)
    {
        $this->contenuAdmin = $contenuAdmin;

        return $this;
    }

    /**
     * Get contenuAdmin
     *
     * @return string
     */
    public function getContenuAdmin()
    {
        return $this->contenuAdmin;
    }

    /**
     * Set variables
     *
     * @param string $variables
     *
     * @return Notification
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;

        return $this;
    }

    /**
     * Get variables
     *
     * @return string
     */
    public function getVariables()
    {
        return $this->variables;
    }
}
