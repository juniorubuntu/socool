<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collecte
 *
 * @ORM\Table(name="collecte")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CollecteRepository")
 */
class Collecte
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=1000, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=1000, nullable=false)
     */
    private $email;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", length=1000, nullable=false)
     */
    private $montant;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCollecte", type="datetime", nullable=false)
     */
    private $dateCollecte;


    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;


    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=1000, nullable=true)
     */
    private $profession;


    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=1000, nullable=true)
     */
    private $pays;


    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=1000, nullable=true)
     */
    private $ville;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Campagne")
     */
    private $campagne;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application")
     */
    private $application;


    /**
     * @var bool
     *
     * @ORM\Column(name="paye", type="boolean", nullable=true)
     */
    private $paye;



    /**
     * @var string
     *
     * @ORM\Column(name="session", type="string", length=1000, nullable=true)
     */
    private $session;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur")
     */
    private $utilisateur;

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Collecte
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Collecte
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set dateCollecte
     *
     * @param \DateTime $dateCollecte
     *
     * @return Collecte
     */
    public function setDateCollecte($dateCollecte)
    {
        $this->dateCollecte = $dateCollecte;

        return $this;
    }

    /**
     * Get dateCollecte
     *
     * @return \DateTime
     */
    public function getDateCollecte()
    {
        return $this->dateCollecte;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Collecte
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return Collecte
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Collecte
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Collecte
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set campagne
     *
     * @param \AppBundle\Entity\Campagne $campagne
     *
     * @return Collecte
     */
    public function setCampagne(\AppBundle\Entity\Campagne $campagne = null)
    {
        $this->campagne = $campagne;

        return $this;
    }

    /**
     * Get campagne
     *
     * @return \AppBundle\Entity\Campagne
     */
    public function getCampagne()
    {
        return $this->campagne;
    }

    /**
     * Set application
     *
     * @param \AppBundle\Entity\Application $application
     *
     * @return Collecte
     */
    public function setApplication(\AppBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \AppBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set paye
     *
     * @param boolean $paye
     *
     * @return Collecte
     */
    public function setPaye($paye)
    {
        $this->paye = $paye;

        return $this;
    }

    /**
     * Get paye
     *
     * @return boolean
     */
    public function getPaye()
    {
        return $this->paye;
    }

    /**
     * Set session
     *
     * @param string $session
     *
     * @return Collecte
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\Utilisateur $utilisateur
     *
     * @return Collecte
     */
    public function setUtilisateur(\UserBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Collecte
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
