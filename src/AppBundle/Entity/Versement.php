<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Versement
 *
 * @ORM\Table(name="versement")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VersementRepository")
 */
class Versement
{

    public function __construct()
    {
        $this->setStatut(false);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", length=1000, nullable=false)
     */
    private $montant;

    /**
     * @var float
     *
     * @ORM\Column(name="retenue", type="float", length=1000, nullable=true)
     */
    private $retenue;

    /**
     * @var float
     *
     * @ORM\Column(name="frais", type="float", length=1000, nullable=true)
     */
    private $frais;

    /**
     * @var float
     *
     * @ORM\Column(name="percevoir", type="float", length=1000, nullable=true)
     */
    private $percevoir;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateDemande", type="datetime", nullable=false)
     */
    private $dateDemande;

    /**
     * @var bool
     *
     * @ORM\Column(name="statut", type="boolean", nullable=true)
     */
    private $statut;

    /**
     * @var bool
     *
     * @ORM\Column(name="commentaire", type="text", length=1000000, nullable=true)
     */
    private $commentaire;

    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur")
     */
    private $utilisateur;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Campagne")
     */
    private $campagne;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Versement
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set retenue
     *
     * @param float $retenue
     *
     * @return Versement
     */
    public function setRetenue($retenue)
    {
        $this->retenue = $retenue;

        return $this;
    }

    /**
     * Get retenue
     *
     * @return float
     */
    public function getRetenue()
    {
        return $this->retenue;
    }

    /**
     * Set percevoir
     *
     * @param float $percevoir
     *
     * @return Versement
     */
    public function setPercevoir($percevoir)
    {
        $this->percevoir = $percevoir;

        return $this;
    }

    /**
     * Get percevoir
     *
     * @return float
     */
    public function getPercevoir()
    {
        return $this->percevoir;
    }

    /**
     * Set dateDemande
     *
     * @param \DateTime $dateDemande
     *
     * @return Versement
     */
    public function setDateDemande($dateDemande)
    {
        $this->dateDemande = $dateDemande;

        return $this;
    }

    /**
     * Get dateDemande
     *
     * @return \DateTime
     */
    public function getDateDemande()
    {
        return $this->dateDemande;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\Utilisateur $utilisateur
     *
     * @return Versement
     */
    public function setUtilisateur(\UserBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set statut
     *
     * @param boolean $statut
     *
     * @return Versement
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get statut
     *
     * @return boolean
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return Versement
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set frais
     *
     * @param float $frais
     *
     * @return Versement
     */
    public function setFrais($frais)
    {
        $this->frais = $frais;

        return $this;
    }

    /**
     * Get frais
     *
     * @return float
     */
    public function getFrais()
    {
        return $this->frais;
    }

    /**
     * Set campagne
     *
     * @param \AppBundle\Entity\Campagne $campagne
     *
     * @return Versement
     */
    public function setCampagne(\AppBundle\Entity\Campagne $campagne = null)
    {
        $this->campagne = $campagne;

        return $this;
    }

    /**
     * Get campagne
     *
     * @return \AppBundle\Entity\Campagne
     */
    public function getCampagne()
    {
        return $this->campagne;
    }
}
