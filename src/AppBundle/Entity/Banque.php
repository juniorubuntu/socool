<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Banque
 *
 * @ORM\Table(name="banque")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BanqueRepository")
 */
class Banque
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $nomBanque;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $nomTitulaire;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=1000, nullable=false)
     */
    private $codeBanque;

    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=1000, nullable=false)
     */
    private $codeGuichet;


    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=1000, nullable=false)
     */
    private $numeroCompte;


    /**
     * @var integer
     * 
     * @ORM\Column(type="integer", length=1000, nullable=false)
     */
    private $cleRIB;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=1000, nullable=false)
     */
    private $IBAN;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur")
     */
    private $utilisateur;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomBanque
     *
     * @param string $nomBanque
     *
     * @return Banque
     */
    public function setNomBanque($nomBanque)
    {
        $this->nomBanque = $nomBanque;

        return $this;
    }

    /**
     * Get nomBanque
     *
     * @return string
     */
    public function getNomBanque()
    {
        return $this->nomBanque;
    }

    /**
     * Set nomTitulaire
     *
     * @param string $nomTitulaire
     *
     * @return Banque
     */
    public function setNomTitulaire($nomTitulaire)
    {
        $this->nomTitulaire = $nomTitulaire;

        return $this;
    }

    /**
     * Get nomTitulaire
     *
     * @return string
     */
    public function getNomTitulaire()
    {
        return $this->nomTitulaire;
    }

    /**
     * Set codeBanque
     *
     * @param integer $codeBanque
     *
     * @return Banque
     */
    public function setCodeBanque($codeBanque)
    {
        $this->codeBanque = $codeBanque;

        return $this;
    }

    /**
     * Get codeBanque
     *
     * @return integer
     */
    public function getCodeBanque()
    {
        return $this->codeBanque;
    }

    /**
     * Set codeGuichet
     *
     * @param integer $codeGuichet
     *
     * @return Banque
     */
    public function setCodeGuichet($codeGuichet)
    {
        $this->codeGuichet = $codeGuichet;

        return $this;
    }

    /**
     * Get codeGuichet
     *
     * @return integer
     */
    public function getCodeGuichet()
    {
        return $this->codeGuichet;
    }

    /**
     * Set numeroCompte
     *
     * @param integer $numeroCompte
     *
     * @return Banque
     */
    public function setNumeroCompte($numeroCompte)
    {
        $this->numeroCompte = $numeroCompte;

        return $this;
    }

    /**
     * Get numeroCompte
     *
     * @return integer
     */
    public function getNumeroCompte()
    {
        return $this->numeroCompte;
    }

    /**
     * Set cleRIB
     *
     * @param integer $cleRIB
     *
     * @return Banque
     */
    public function setCleRIB($cleRIB)
    {
        $this->cleRIB = $cleRIB;

        return $this;
    }

    /**
     * Get cleRIB
     *
     * @return integer
     */
    public function getCleRIB()
    {
        return $this->cleRIB;
    }

    /**
     * Set iBAN
     *
     * @param string $iBAN
     *
     * @return Banque
     */
    public function setIBAN($iBAN)
    {
        $this->IBAN = $iBAN;

        return $this;
    }

    /**
     * Get iBAN
     *
     * @return string
     */
    public function getIBAN()
    {
        return $this->IBAN;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\Utilisateur $utilisateur
     *
     * @return Banque
     */
    public function setUtilisateur(\UserBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }
}
