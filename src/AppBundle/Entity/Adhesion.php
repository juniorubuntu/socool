<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Adhesion
 *
 * @ORM\Table(name="adhesion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdhesionRepository")
 */
class Adhesion
{

    public function _construct()
    {
        $this->setMontant(0);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=1000, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=1000, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=false)
     */
    private $telephone;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateAdhesion", type="datetime", nullable=false)
     */
    private $dateAdhesion;

    /**
     * @var string
     *
     * @ORM\Column(name="typeMembre", type="string", length=1000, nullable=false)
     */
    private $typeMembre;


    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=1000, nullable=false)
     */
    private $profession;
    

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=1000, nullable=true)
     */
    private $pays;


    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=1000, nullable=true)
     */
    private $ville;


    /**
     * @var integer
     *
     * @ORM\Column(name="age", type="integer", length=255, nullable=true)
     */
    private $age;


    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=1000, nullable=true)
     */
    private $sexe;


    /**
     * @var string
     *
     * @ORM\Column(name="motivation", type="text", nullable=true)
     */
    private $motivation;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Campagne")
     */
    private $campagne;


    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Application")
     */
    private $application;

    /**
     * @var object
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\Utilisateur")
     */
    private $utilisateur;


    /**
     * @var bool
     *
     * @ORM\Column(name="paye", type="boolean", nullable=true)
     */
    private $paye;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="float", length=1000, nullable=true)
     */
    private $montant;


    /**
     * @var string
     *
     * @ORM\Column(name="session", type="string", length=1000, nullable=true)
     */
    private $session;


    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Adhesion
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Adhesion
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set dateAdhesion
     *
     * @param \DateTime $dateAdhesion
     *
     * @return Adhesion
     */
    public function setDateAdhesion($dateAdhesion)
    {
        $this->dateAdhesion = $dateAdhesion;

        return $this;
    }

    /**
     * Get dateAdhesion
     *
     * @return \DateTime
     */
    public function getDateAdhesion()
    {
        return $this->dateAdhesion;
    }

    /**
     * Set typeMembre
     *
     * @param string $typeMembre
     *
     * @return Adhesion
     */
    public function setTypeMembre($typeMembre)
    {
        $this->typeMembre = $typeMembre;

        return $this;
    }

    /**
     * Get typeMembre
     *
     * @return string
     */
    public function getTypeMembre()
    {
        return $this->typeMembre;
    }

    /**
     * Set profession
     *
     * @param string $profession
     *
     * @return Adhesion
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Adhesion
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param string $ville
     *
     * @return Adhesion
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set age
     *
     * @param integer $age
     *
     * @return Adhesion
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get age
     *
     * @return integer
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set motivation
     *
     * @param string $motivation
     *
     * @return Adhesion
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;

        return $this;
    }

    /**
     * Get motivation
     *
     * @return string
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * Set campagne
     *
     * @param \AppBundle\Entity\Campagne $campagne
     *
     * @return Adhesion
     */
    public function setCampagne(\AppBundle\Entity\Campagne $campagne = null)
    {
        $this->campagne = $campagne;

        return $this;
    }

    /**
     * Get campagne
     *
     * @return \AppBundle\Entity\Campagne
     */
    public function getCampagne()
    {
        return $this->campagne;
    }

    /**
     * Set application
     *
     * @param \AppBundle\Entity\Application $application
     *
     * @return Adhesion
     */
    public function setApplication(\AppBundle\Entity\Application $application = null)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * Get application
     *
     * @return \AppBundle\Entity\Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set paye
     *
     * @param boolean $paye
     *
     * @return Adhesion
     */
    public function setPaye($paye)
    {
        $this->paye = $paye;

        return $this;
    }

    /**
     * Get paye
     *
     * @return boolean
     */
    public function getPaye()
    {
        return $this->paye;
    }

    /**
     * Set montant
     *
     * @param float $montant
     *
     * @return Adhesion
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Adhesion
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set session
     *
     * @param string $session
     *
     * @return Adhesion
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set utilisateur
     *
     * @param \UserBundle\Entity\Utilisateur $utilisateur
     *
     * @return Adhesion
     */
    public function setUtilisateur(\UserBundle\Entity\Utilisateur $utilisateur = null)
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return \UserBundle\Entity\Utilisateur
     */
    public function getUtilisateur()
    {
        return $this->utilisateur;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Adhesion
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
