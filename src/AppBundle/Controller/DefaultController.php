<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserBundle\Entity\Utilisateur;
use AppBundle\Entity\Notification;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class DefaultController extends FunctionController {

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request) {

      $em = $this->getDoctrine()->getManager();

      if($this->getUser() && $this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN'){
        $applications = $em->getRepository('AppBundle:Application')->findBy([
          'utilisateur' => $this->getUser()
        ]);

        if(count($applications) == 0){
          return $this->render('template/welcome.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
            'applications' => $applications
          ]);
        }
      }else{

        $applications = $em->getRepository('AppBundle:Application')->findAll();
      }
      $versements = $em->getRepository('AppBundle:Versement')->findBy([
        'statut' => false
      ]);


        // On calcule les statss
      $adhesions ='';
      $collectes ='';
      $montantCollecte ='';
      $montantAdhesion ='';
      $utilisateurs ='';
      $formulaires ='';
      $campagnes ='';
      $applicationsAll ='';

      $this->buildStats($adhesions, $collectes, $formulaires, $campagnes, $applicationsAll, $montantCollecte, $montantAdhesion);

      return $this->render('home.html.twig', [
        'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        'applications' => $applications,
        'adhesions' => $adhesions,
        'collectes' => $collectes,
        'montantCollecte' => $montantCollecte,
        'montantAdhesion' => $montantAdhesion,
        'utilisateurs' => $utilisateurs,
        'formulaires' => $formulaires,
        'campagnes' => $campagnes,
        'applicationsAll' => $applicationsAll,
        'versementsT' => $versements

      ]);
    }


    /**
     * @Route("/paiement-via-carte-bancaire-stripe/{id}/", name="handle_stripe")
     */
    public function handleStripeForm(Request $request, $transfert){

        //On récupère les API dans le fichier config
      $api_key_stripe_secret = $this->container->getParameter('api_key_stripe_secret');
      $api_key_stripe_pulic = $this->container->getParameter('api_key_stripe_pulic');

      require_once __DIR__.'/../../../web/stripe/init.php';
      \Stripe\Stripe::setApiKey($api_key_stripe_secret);
      $token  = $_POST['stripeToken'];
      $email  = $_POST['stripeEmail'];

      $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
      ));

      $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $transfert->getMontant() * 100,
        'currency' => 'eur',
        'description' => 'Transfert d\'argent à '.$transfert->getRecepteur(),
        'receipt_email' => $email  
      ));

      return $this->redirectToRoute('success_pay');
    }

    /**
     * @Route("/paiement/pay/annule/{type}/7626hggz87{id}129JJHADj", name="cancel_pay")
     */
    public function cancelPayPal(Request $request, $type, $id) {
      $em = $this->getDoctrine()->getManager();
      $user = $this->getUser();
      if($type == "COLLECTE"){
        $collecte = $em->getRepository('AppBundle:Collecte')->find($id);
        if($collecte){
          $em->remove($collecte);
          $em->flush();
          $request->getSession()
          ->getFlashBag()
          ->add('echec', 'Transaction annulée');
        }
        return $this->redirectToRoute('homepage');
      }else{
        $adhesion = $em->getRepository('AppBundle:Adhesion')->find($id);
        if($adhesion){
          $em->remove($adhesion);
          $em->flush();
          $request->getSession()
          ->getFlashBag()
          ->add('echec', 'Transaction annulée');
        }
        return $this->redirectToRoute('homepage');
      }
    }

    /**
     * @Route("/paiement/pay/success/{type}/728hjch26{id}2732jhdvd", name="success_pay")
     * @Method({"GET", "POST"})
     */
    public function successPay(Request $request, $type, $id) {
      $em = $this->getDoctrine()->getManager();

      if($type == "COLLECTE"){
        $collecte = $em->getRepository('AppBundle:Collecte')->find($id);

        if($collecte){
          $collecte->setPaye(true);
          $em->flush();

          $request->getSession()
          ->getFlashBag()
          ->add('success', '<h2>Merci pour votre contribution</h2><br>'.$collecte->getApplication().' Vous sera très reconnaissant(e).');

        }else{
          $request->getSession()
          ->getFlashBag()
          ->add('echec', 'Erreur lors du traitement!! <br>L\'argent n\'a pas pu être déposé chez le bénéficiaire. Si vous avez été néanmoins débités, contactez-nous pour régularisation.');
        }

      }elseif($type == "ADHESION") {
        $adhesion = $em->getRepository('AppBundle:Adhesion')->find($id);

        if($adhesion){
          $adhesion->setPaye(true);
          $em->flush();

          $request->getSession()
          ->getFlashBag()
          ->add('success', '<h2>Merci pour votre adhésion</h2><br>'.$adhesion->getApplication().' Vous contactera très prochainement pour les modalités de collaboration future.');

        }else{
          $request->getSession()
          ->getFlashBag()
          ->add('echec', 'Erreur lors du traitement!! <br>L\'argent n\'a pas pu être déposé chez le bénéficiaire. Si vous avez été néanmoins débités, contactez-nous pour régularisation.');
        }
      }

      return $this->redirectToRoute('tempPage');
    }

    /**
     * @Route("/paiement/temporaire/page", name="tempPage")
     */
    public function tempPage(){

      return $this->render('partial/tmp.html.twig');
    }

    /**
    * @Route(
    *   "/connexion", 
    *   defaults={
    *       "locale": "fr",
    *   },
    *   requirements={
    *       "locale": "en|fr",
    *   },
    *   name="homepage_connexion"
    * )
    */
    public function connexionAction(Request $request, $locale) {
      $em = $this->getDoctrine()->getManager();

      $user = $this->getUser();

      if($user->getLevel()->getRightToken() == 'ROLE_CLIENT'){
        return $this->redirectToRoute('homepage');
      }else{
        return $this->redirectToRoute('homepage');
      }
    }

    public function loginPrepare($csrf_token, $error, $request){

    //Formulaire login
      /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
      $session = $request->getSession();

      $authErrorKey = Security::AUTHENTICATION_ERROR;
      $lastUsernameKey = Security::LAST_USERNAME;

    // get the error if any (works with forward and redirect -- see below)
      if ($request->attributes->has($authErrorKey)) {
        $error = $request->attributes->get($authErrorKey);
      } elseif (null !== $session && $session->has($authErrorKey)) {
        $error = $session->get($authErrorKey);
        $session->remove($authErrorKey);
      } else {
        $error = null;
      }

      if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
          }

        // last username entered by the user
          $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

          $csrfToken = $this->has('security.csrf.token_manager')
          ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
          : null;

        }

    /**
     * 
     *
     * @Route("/compte/init/authentification/{messageCreation}", name="authentification")
     * @Method({"GET", "POST"})
     */
    public function vueAuthAction(Request $request, $messageCreation = null)
    {
      $em = $this->getDoctrine()->getManager();

        //Préparation du formulaire de login Client
      $csrfToken = '';
      $error = '';
      $this->loginPrepare($csrfToken, $error, $request);

      $csrfToken = $this->has('security.csrf.token_manager')
      ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
      : null;

        //Formulaire création de compte
      $utilisateur = new Utilisateur();
      $utilisateur->setLevel($em->getRepository('UserBundle:Droit')->findOneBy(['rightToken'=> 'ROLE_CLIENT']));
      $utilisateur->setEnabled(FALSE);
      $utilisateur->setUsername('U');

      $form = $this->createForm('UserBundle\Form\UtilisateurType', $utilisateur);
      $form->remove('level');
      $form->remove('pays');
      $form->remove('username');
      $form->remove('telephone');
      $form->remove('cni');
      $form->remove('sexe');
      $form->remove('codePostal');
      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {

            //Check username
        $userExist = $em->getRepository('UserBundle:Utilisateur')->findOneBy(['username'=> $utilisateur->getUsername()]);
        if($userExist == NULL){
          $userExist = $em->getRepository('UserBundle:Utilisateur')->findOneBy(['email'=> $utilisateur->getEmail()]);
          if($userExist != NULL){
                //Flasbag message
            $request->getSession()
            ->getFlashBag()
            ->add('echec', 'Un compte avec cet adresse email existe déjà. Connectez-vous si l\'adresse est à vous!');
          }
        }else{
          $request->getSession()
          ->getFlashBag()
          ->add('echec', 'Votre nom d\'utilisateur est déjà utilisé.');
        }
        if($userExist != NULL){

          return $this->render('template/login.html.twig', array(
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
            'error' => $error
          ));
        }
        $utilisateur->setUsername($utilisateur->getEmail());
        $utilisateur->setSession($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
        $utilisateur->setRoles(array('ROLE_CLIENT'));
        $tokenGenerator = $this->get('fos_user.util.token_generator');
        $utilisateur->setConfirmationToken($tokenGenerator->generateToken());
        $em->persist($utilisateur);
        $em->flush();

        $this->sendMailConfirm($utilisateur);

        return $this->redirectToRoute('code_verificaion', ['email' => $utilisateur->getEmail()]);
      }

      $compte = 'non';

      if($form->isSubmitted() && !$form->isValid()){
        $request->getSession()
        ->getFlashBag()
        ->add('echec', 'Les données du formulaire sont incorrectes');
        $compte = 'oui';
      }

      if(isset($_GET['compte'])){
        $compte = 'oui';
      }

      return $this->render('template/login.html.twig', array(
        'form' => $form->createView(),
        'csrf_token' => $csrfToken,
        'error' => $error,
        'compte' => $compte,
        'session' => $_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT'],
        'messageCreation' => $messageCreation
      ));
    }


    /**
     * @Route("/validation-du-compte/{email}/renvoie-du-mail-{renvoie}", name="code_verificaion")
     */
    public function validCompte(Request $request, $email, $renvoie = null) {

      $em = $this->getDoctrine()->getManager();

      $messageConfirm = '<h2 class="info-compte" style="font-size: 18px;font-weight: bold;background: #c90000;padding: 7px;margin-bottom: 11px;">Votre compte a été créé avec succès</h2>';
      if($renvoie != null){
        $messageConfirm .= '<b style="color:#060609">Mail renvoyé avec succès</b><br>';
        $utilisateur = $em->getRepository('UserBundle:Utilisateur')->findOneBy(['email'=> $email]);
        $this->sendMailConfirm($utilisateur);
      }

      //Notif message
      $messageConfirm .= 'Pour terminer l\'opération, cliquez sur le lien reçu par mail à <b>'.$email.'</b><br>Vérifiez vos "spams" si le mail n\'est pas dans votre boite de réception.';

      return $this->render('partial/login/codeVerif.html.twig', [
        'email' => $email,
        'messageConfirm' => $messageConfirm
      ]);

    }


    /**
     * @Route("/activation-du-compte-/{token}", name="activation_compte")
     */
    public function activCompte(Request $request,  $token) {
      $em = $this->getDoctrine()->getManager();
      $user = $em->getRepository('UserBundle:Utilisateur')->findOneBy([
        'confirmationToken' => $token
      ]);

      if($user){
        $user->setEnabled(true);
        $em->flush();
        $messageCreation = 'Votre compte a été validé avec succès. Vous pouvez maintenant vous connecter.';
      }
      return $this->redirectToRoute('authentification', ['messageCreation' => $messageCreation]);
    }


    /**
     * This function is made to enable a user account.
     *
     * @Route("/compte/mon-profil", name="utilisateur_profil")
     * 
     */
    public function monProfile(Request $request) {

      $em = $this->getDoctrine()->getManager();
      $user = $this->getUser();
      if($user){
        return $this->render('utilisateur/profil/mon-profil.html.twig', array(
          'utilisateur' => $user
        ));
      }

      $request->getSession()
      ->getFlashBag()
      ->add('info', 'Vous devez vous connecter pour accéder à cette section.');
      return $this->redirectToRoute('homepage');
    }


    /**
     * @Route("/recharge-via-carte-bancaire-stripe-de-{montant}/", name="handle_stripe_recharge")
     */
    public function handleStripeRechargeForm(Request $request, $montant){
        //On récupère les API dans le fichier config
      $api_key_stripe_secret = $this->container->getParameter('api_key_stripe_secret');
      $api_key_stripe_pulic = $this->container->getParameter('api_key_stripe_pulic');

      require_once __DIR__.'/../../../web/stripe/init.php';
      \Stripe\Stripe::setApiKey("sk_test_51IGMjnI2aY5ZKjXZZJF2TusonvRPmkMcw2emlSGnAmgLWZhcPRgWfnoDjKNkWc89tI98nLsdYzQYot0NUWF5mnRf003YgvioOY");
      $token  = $_POST['stripeToken'];
      $email  = $_POST['stripeEmail'];

      $customer = \Stripe\Customer::create(array(
        'email' => $email,
        'source'  => $token
      ));

      $charge = \Stripe\Charge::create(array(
        'customer' => $customer->id,
        'amount'   => $montant * 100,
        'currency' => 'eur',
        'description' => 'Recharge de fonds',
        'receipt_email' => $email  
      ));

      return $this->redirectToRoute('success_pay_recharge', array('montant' => $montant));
    }


    /**
     * @Route("/users/creer-son-compte", name="new_compte_user")
     * @Method({"POST", "GET"})
     */
    public function newUserAccount(Request $request){
      $em = $this->getDoctrine()->getManager();

      $csrfToken = $this->has('security.csrf.token_manager')
      ? $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue()
      : null;
        //Formulaire création de compte
      $utilisateur = new Utilisateur();
      $utilisateur->setLevel($em->getRepository('UserBundle:Droit')->findOneBy(['rightToken'=> 'ROLE_CLIENT']));
      $utilisateur->setEnabled(FALSE);
      $utilisateur->setUsername('U');

      $form = $this->createForm('UserBundle\Form\UtilisateurType', $utilisateur);
      $form->remove('organisation');
      $form->remove('ville');
      $form->remove('level');
      $form->remove('sexe');
      $form->remove('username');
      $form->add('codePostal');
      $form->add('pays');
      $form->add('cni', FileType::class, ['required' => false]);
        /*$form->add('date', DateType::class, 
            array(
                'widget' => 'single_text'
            )
          );*/
          $form->handleRequest($request);

          if ($form->isSubmitted() && $form->isValid()) {

            //Check username
            $userExist = $em->getRepository('UserBundle:Utilisateur')->findOneBy(['username'=> $utilisateur->getUsername()]);
            if($userExist == NULL){
              $userExist = $em->getRepository('UserBundle:Utilisateur')->findOneBy(['email'=> $utilisateur->getEmail()]);
              if($userExist != NULL){
                //Flasbag message
                $request->getSession()
                ->getFlashBag()
                ->add('echec', 'un compte avec cet adresse email existe déjà');
              }
            }else{
              $request->getSession()
              ->getFlashBag()
              ->add('echec', 'Votre nom d\'utilisateur est déjà utilisé');
            }
            if($userExist != NULL){
              return $this->render('utilisateur/profil/new-account.html.twig', array(
                'utilisateur' => $utilisateur,
                'form' => $form->createView(),
                'csrf_token' => $csrfToken,
              ));
            }

            $elements = $request->request->all();
            //On teste d'abord s'il a soumis un fichier avant de tenter l'importation
            if(!empty($_FILES['userbundle_utilisateur']['name']['cni'])){
              $fichier = '';
              $cni = $this->SingleImportation($fichier);
              $utilisateur->setCni($cni);
            }

            $utilisateur->setUsername($utilisateur->getEmail());
            $utilisateur->setRoles(array('ROLE_CLIENT'));
            $utilisateur->setSession($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $utilisateur->setConfirmationToken($tokenGenerator->generateToken());

            $code = $this->chaineAleatoire();
            $utilisateur->setCode($code);
            $em->persist($utilisateur);
            $em->flush();

            //Flasbag message
            $request->getSession()
            ->getFlashBag()
            ->add('success', ' Compte utilisateur créé avec succès. Vous devez valider votre compte en entrant le code qui a été envoyé à la boite mail "'.$utilisateur->getEmail().'". <br>Vérifiez vos "spams" si le mail n\'est pas dans votre boite de réception et ajoutez l\'adresse mail de KOBA à la liste de vos contacts.</b>');
            $this->sendMailConfirm($utilisateur, $code);

            return $this->redirectToRoute('code_verificaion');
          }

          return $this->render('utilisateur/profil/new-account.html.twig', array(
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
            'csrf_token' => $csrfToken,
          ));
        }


    /**
     * @Route("/users/Je-modifie-ma-cni-passport", name="user_edit_cni")
     * @Method({"POST", "GET"})
     */
    public function editCniUser(Request $request){
      $em = $this->getDoctrine()->getManager();
      $elements = $request->request->all();
      if(isset($elements['modifier']) && !empty($_FILES['userbundle_utilisateur']['name']['cni'])){
        $user = $this->getUser();

        $dossier = $this->get('kernel')->getRootDir() . '/../web/Uploads/cni/';
        $image = '';
        $image = $this->SingleImportation($image);


        $OldImage = $user->getCni();
        $user->setCni($image);

        $this->getDoctrine()->getManager()->flush();

        if($OldImage != '' && file_exists($dossier.$OldImage)) {
          unlink($dossier.$OldImage);
        }
        $em->persist($user);
        $em->flush();

        $request->getSession()
        ->getFlashBag()
        ->add('success', 'Votre CNI/PASSPORT a été mis à jour avec succès.');

        return $this->redirectToRoute('homepage');
      }elseif(isset($elements['modifier'])){
        $request->getSession()
        ->getFlashBag()
        ->add('echec', 'Aucun fichier à importer.');
      }

      return $this->render('utilisateur/profil/edit-cni.html.twig', array(

      ));
    }


    /**
     * Page login.
     *
     * @Route("/compte/login", name="user_login_page")
     * 
     */
    public function monLogin() {

      return $this->render('template/login.html.twig');

    }


    /**
     * Gestion des intégrations et APIs
     *
     * @Route("/api/integration/config", name="api_config")
     * 
     */
    public function apiAction() {

      return $this->render('apis/configuration.html.twig');

    }


  }
