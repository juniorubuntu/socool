<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UserBundle\Entity\Utilisateur;
use AppBundle\Entity\Commission;
use AppBundle\Entity\Notification;

class FunctionController extends Controller {

    public function sendMailTrans(Notification $notif, $contenuClient, $contenuAdmin, $emailClient){
        //Initialisation du message client
        $message = \Swift_Message::newInstance()
        ->setFrom('contact@socollect.org')
        ->setCc('copie@socollect.org')
        ->setTo($emailClient)
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject($notif->getSujetClient())
        ->setBody($this->renderView('mails/mailEnvoie.html.twig', array(
            'contenu' => $contenuClient
        )));

            //Envoie du message
        $this->get('mailer')->send($message);

        //Initialisation du message admin
        $message = \Swift_Message::newInstance()
        ->setFrom('contact@socollect.org')
        ->setCc('copie@socollect.org')
        ->setTo(['transfert@koba.cm', 'juniorubuntu54@gmail.com'])
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject($notif->getSujetAdmin())
        ->setBody($this->renderView('mails/mailEnvoie.html.twig', array(
            'contenu' => $contenuAdmin
        )));

            //Envoie du message
        $this->get('mailer')->send($message);
    }

    public function sendMailVers(Notification $notif = null, $contenuClient, $contenuAdmin, $emailClient){
        //Initialisation du message client
        $message = \Swift_Message::newInstance()
        ->setFrom('contact@socollect.org')
        ->setCc('copie@socollect.org')
        ->setTo($emailClient)
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject($notif->getSujetClient())
        ->setBody($this->renderView('mails/mailEnvoie.html.twig', array(
            'contenu' => $contenuClient
        )));

            //Envoie du message
        $this->get('mailer')->send($message);

        //Initialisation du message admin
        $message = \Swift_Message::newInstance()
        ->setFrom('contact@socollect.org')
        ->setCc('copie@socollect.org')
        ->setTo(['transfert@koba.cm', 'juniorubuntu54@gmail.com'])
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject($notif->getSujetAdmin())
        ->setBody($this->renderView('mails/mailEnvoie.html.twig', array(
            'contenu' => $contenuAdmin
        )));

            //Envoie du message
        $this->get('mailer')->send($message);
    }

    public function configSendMail($contenu, $transfert){

        $contenu = str_replace("{{nom_client}}", $transfert->getUtilisateur(), $contenu);
        $contenu = str_replace("{{email_client}}",$transfert->getUtilisateur()->getEmail(), $contenu);
        $contenu = str_replace("{{pays_client}}",$transfert->getUtilisateur()->getPays(), $contenu);
        $contenu = str_replace("{{montant}}",$transfert->getMontant(), $contenu);
        $contenu = str_replace("{{devise}}",$transfert->getDevise(), $contenu);
        $contenu = str_replace("{{num_reference}}", 'KB#'.$transfert->getId(), $contenu);
        $contenu = str_replace("{{date_envoie}}", $transfert->getDate()->format('d-m-Y'), $contenu);
        $contenu = str_replace("{{nom_beneficiare}}", $transfert->getRecepteur(), $contenu);
        $contenu = str_replace("{{telephone_beneficiare}}", $transfert->getRecepteur()->getNumero(), $contenu);
        $contenu = str_replace("{{net_a_percevoir}}", $transfert->getPercevoir(), $contenu);

        return $contenu;
    }

    public function configLitigeMail($contenu, $litige, Reponse $reponse = null){

        $contenu = str_replace("{{nom_client}}", $litige->getTransfert()->getUtilisateur(), $contenu);
        $contenu = str_replace("{{beneficiaire}}", $litige->getTransfert()->getRecepteur(), $contenu);
        $contenu = str_replace("{{sujet_litige}}", $litige->getSujet(), $contenu);
        $contenu = str_replace("{{montant}}", $litige->getTransfert()->getMontant(), $contenu);
        $contenu = str_replace("{{devise}}", $litige->getTransfert()->getDevise(), $contenu);
        $contenu = str_replace("{{ref_transfert}}", 'KB#'.$litige->getTransfert()->getId(), $contenu);
        $contenu = str_replace("{{date_transfert}}", $litige->getTransfert()->getDate()->format('d-m-Y'), $contenu);
        $contenu = str_replace("{{net_a_percevoir}}", $litige->getTransfert()->getPercevoir(), $contenu);
        $contenu = str_replace("{{contenu_litige}}", $litige->getDescription(), $contenu);
        $contenu = str_replace("{{contenu_reponse}}", ($reponse == null)? 'RAS' : $reponse->getContenu(), $contenu);

        return $contenu;
    }

    public function configRechargeMail($contenu, $montant, $historique, $devise, Utilisateur $user){

        $contenu = str_replace("{{nom_client}}", $user, $contenu);
        $contenu = str_replace("{{montant}}", $montant, $contenu);
        $contenu = str_replace("{{devise}}", $devise, $contenu);
        $contenu = str_replace("{{date_recharge}}",$historique->getDate()->format('d-m-Y'), $contenu);
        $contenu = str_replace("{{nouveau_solde}}", $user->getFonds(), $contenu);

        return $contenu;
    }

    public function configVirementMail($contenu, $montant, $virement, $devise, Utilisateur $user){

        $contenu = str_replace("{{nom_client}}", $user, $contenu);
        $contenu = str_replace("{{nom_beneficiaire}}", $virement->getBeneficiaire(), $contenu);
        $contenu = str_replace("{{montant}}", $montant, $contenu);
        $contenu = str_replace("{{devise}}", $devise, $contenu);
        $contenu = str_replace("{{date_virement}}",$virement->getDate()->format('d-m-Y'), $contenu);
        $contenu = str_replace("{{ref_virement}}", 'KBVR#'.$virement->getId(), $contenu);

        return $contenu;
    }


    public function sendMailConfirm($user){
        //Création du lien de modification du mot de passe
        $url = $this->generateUrl('activation_compte', array(
            'token'=> $user->getConfirmationToken(),
        ));

        //Initialisation du message
        $message = \Swift_Message::newInstance()
        ->setFrom('contact@soCollect.org')
        ->setCc('copie@soCollect.org')
        ->setTo($user->getEmail())
        ->setCharset('utf-8')
        ->setContentType('text/html')
        ->setSubject("soCollect - Validation du compte")
        ->setBody($this->renderView('mails/mailCompte.html.twig', array(
            'user' => $user,
            'confirmationUrl' => $url
        )));

        //Envoie du message
        $this->get('mailer')->send($message);
    }

    public function chaineAleatoire(){
        $alfa='ABCDEFGHIJKLMNPQRSTUVWXYZ0123456789';
        return substr(str_shuffle($alfa),0,4);
    }


    public function builPayStripeId($montant, $user, $collecte = null, $adhesion = null, $productType){
        //On charge une fois stripe au cas où il cliquerait dessus
        $api_key_stripe_secret = $this->container->getParameter('api_key_stripe_secret');
        require_once __DIR__.'/../../../web/stripe/init.php';
        \Stripe\Stripe::setApiKey($api_key_stripe_secret);

        if($_SERVER['SERVER_NAME'] == "localhost"){
            $lien = "http://localhost/soCool/web/Uploads/logos/";
        }else{
            $lien = 'https://'.$_SERVER['SERVER_NAME'].'/web/Uploads/logos/';
        }

        if($productType == 'COLLECTE'){
            $type = "COLLECTE";
            $id = $collecte->getId();
            $montant = $collecte->getMontant();

            //Création de l'objet produit
            $product = \Stripe\Product::create([
              'name' => $collecte->getCampagne(),
              'description' => 'Versement de '.$collecte->getMontant().'€ pour '.$collecte->getCampagne()->getNom().' à '.$collecte->getApplication()->getNom(),
              'images' => [$lien.$collecte->getApplication()->getLogo()],
          ]);
        }else{
            $type = "ADHESION";
            $id = $adhesion->getId();
            $montant = $adhesion->getMontant();

            //Création de l'objet produit
            $product = \Stripe\Product::create([
              'name' => $adhesion->getCampagne(),
              'description' => 'Versement de '.$adhesion->getMontant().'€ pour '.$adhesion->getCampagne()->getNom().' à '.$adhesion->getApplication()->getNom(),
              'images' => [$lien.$adhesion->getApplication()->getLogo()],
          ]);
        }

        $lienRetour = $this->container->getParameter('url_retour');
        $lienEchec = $this->container->getParameter('url_echec');

        //On met le type dans l'URL
        $lienRetour = str_replace("TYPE", $type, $lienRetour);
        $lienEchec = str_replace("TYPE", $type, $lienEchec);

        //On met l'ID dans l'URL
        $lienRetour = str_replace("ID", $id, $lienRetour);
        $lienEchec = str_replace("ID", $id, $lienEchec);
        

        $session = \Stripe\Checkout\Session::create([
          'payment_method_types' => ['card'],
          'line_items' => [[
            'price_data' => [
              'product' => $product->id,
              'unit_amount' => $montant * 100,
              'currency' => 'eur',
          ],
          'quantity' => 1,
        ]],
          'mode' => 'payment',
          'customer_email'=> $user ? $user->getEmail() : 'exemple@domain.com' ,
          'success_url' => $lienRetour,
          'cancel_url' => $lienEchec,
        ]);
        return $session->id;
    }

    public function findApplications(){
        $em = $this->getDoctrine()->getManager();

        if($this->getUser() && $this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN'){
            $applications = $em->getRepository('AppBundle:Application')->findBy([
                'utilisateur' => $this->getUser()
            ]);
        }else{
            $applications = $em->getRepository('AppBundle:Application')->findAll();
        }

        return $applications;
    }


    public function buildStats(&$adhesions, &$collectes, &$formulaires, &$campagnes, &$applicationsAll, &$montantCollecte, &$montantAdhesion)
    {

        $em = $this->getDoctrine()->getManager();

        if($this->getUser()->getLevel()->getRightToken() == "ROLE_ADMIN"){
            // On calcule les stats
            $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
              'paye' => true
          ]);

            $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
              'paye' => true
          ]);
            $montantCollecte = 0;
            foreach ($collectes as $collecte) {
                $montantCollecte += $collecte->getMontant();
            }

            $levelUser = $em->getRepository('UserBundle:Droit')->findOneBy([
              'rightToken' => 'ROLE_CLIENT',
          ]);

            $utilisateurs = $em->getRepository('UserBundle:Utilisateur')->findBy([
              'level' => $levelUser
          ]);

            $formulaires = $em->getRepository('AppBundle:Formulaire')->findAll();

            $campagnes = $em->getRepository('AppBundle:Campagne')->findAll();

            $applicationsAll = $em->getRepository('AppBundle:Application')->findAll();
        }else{
            // On calcule les stats
            $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
                'paye' => true,
                'utilisateur' => $this->getUser()
          ]);

            $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
              'paye' => true,
              'utilisateur' => $this->getUser()
          ]);
            $montantCollecte = 0;
            foreach ($collectes as $collecte) {
                $montantCollecte += $collecte->getMontant();
            }

            $montantAdhesion = 0;
            foreach ($adhesions as $adhesion) {
                $montantAdhesion += $adhesion->getMontant();
            }

            $levelUser = $em->getRepository('UserBundle:Droit')->findOneBy([
              'rightToken' => 'ROLE_CLIENT',
          ]);

            $utilisateurs = $em->getRepository('UserBundle:Utilisateur')->findBy([
              'level' => $levelUser
          ]);

            $formulaires = $em->getRepository('AppBundle:Formulaire')->findBy([
                'utilisateur' => $this->getUser()
            ]);

            $campagnes = $em->getRepository('AppBundle:Campagne')->findBy([
                'utilisateur' => $this->getUser()
            ]);

            $applicationsAll = $em->getRepository('AppBundle:Application')->findBy([
                'utilisateur' => $this->getUser()
            ]);
        }
    }
    


    public function SingleImportation($fichier, $sousDossier) {
        ini_set('memory_limit', '256M');
        $dossier = $this->get('kernel')->getRootDir() . '/../web/Uploads/'.$sousDossier.'/';
        // importation du fichier
        //$total_fichier_uploade = count($_FILES['appbundle_application']['name']['image']);
        $extensions = array('.jpg', '.png', '.jpeg');
        $taille_maxi = 1000000;
        $fichier = basename($_FILES['appbundle_application']['name']['logo']);
        $taille = filesize($_FILES['appbundle_application']['tmp_name']['logo']);
        $extension = strrchr($_FILES['appbundle_application']['name']['logo'], '.');
        //Début des vérifications de sécurité...
        if (!in_array($extension, $extensions)) { //Si l'extension n'est pas dans le tableau
        $erreur = 'Vous devez uploader un fichier de type image/jpg, image/png, image/jpeg';
    }

    if ($taille > $taille_maxi) {
        $erreur = 'Le fichier est trop gros...';
    }
        if (!isset($erreur)) { //S'il n'y a pas d'erreur, on upload
            //On formate le nom du fichier ici...
        $fichier = strtr($fichier, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

            //On prends la date d'enregistrement
        $date = \DateTime::createFromFormat("Y-m-d\TH:i:s", date('Y-m-d\TH:i:s'));

            //On format le nom du fichier avec la date pour garantir l'unicité
        $fichier = $date->format('Y-m-d\TH:i:s').'__'.$fichier;
            if (move_uploaded_file($_FILES['appbundle_application']['tmp_name']['logo'], $dossier . $fichier)) { //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
                //echo 'Upload Fichier ';
                //echo ''. $i + 1;

            } else { //Sinon (la fonction renvoie FALSE).
                echo 'Echec upload de la pièce jointe';
            }

        } else {
            echo $erreur;
            die();
        }
        //return $fichier;
        return $fichier;
    }

    public function SingleImportationCouverture($fichier, $sousDossier) {

        ini_set('memory_limit', '256M');
        $dossier = $this->get('kernel')->getRootDir() . '/../web/Uploads/'.$sousDossier.'/';
        // importation du fichier
        //$total_fichier_uploade = count($_FILES['appbundle_campagne']['name']['image']);
        $extensions = array('.jpg', '.png', '.jpeg');
        $taille_maxi = 1000000;
        $fichier = basename($_FILES['appbundle_campagne']['name']['image']);
        $taille = filesize($_FILES['appbundle_campagne']['tmp_name']['image']);
        $extension = strrchr($_FILES['appbundle_campagne']['name']['image'], '.');
        //Début des vérifications de sécurité...
        if (!in_array($extension, $extensions)) { //Si l'extension n'est pas dans le tableau
            $erreur = 'Vous devez uploader un fichier de type image/jpg, image/png, image/jpeg';
        }

        if ($taille > $taille_maxi) {
            $erreur = 'Le fichier est trop gros...';
        }
        if (!isset($erreur)) { //S'il n'y a pas d'erreur, on upload
            //On formate le nom du fichier ici...
        $fichier = strtr($fichier, 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
        $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

            //On prends la date d'enregistrement
        $date = \DateTime::createFromFormat("Y-m-d\TH:i:s", date('Y-m-d\TH:i:s'));

            //On format le nom du fichier avec la date pour garantir l'unicité
        $fichier = $date->format('Y-m-d\TH:i:s').'__'.$fichier;
            if (move_uploaded_file($_FILES['appbundle_campagne']['tmp_name']['image'], $dossier . $fichier)) { //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
                //echo 'Upload Fichier ';
                //echo ''. $i + 1;

            } else { //Sinon (la fonction renvoie FALSE).
                echo 'Echec upload de la pièce jointe';
            }

        } else {
            echo $erreur;
            die();
        }
        //return $fichier;
        return $fichier;
    }


    public function buildSolde(Utilisateur $utilisateur)
    {
        $em = $this->getDoctrine()->getManager();

        $montantCollecte = 0;
        $montantAdhesion = 0;
        $ancienRetrait = 0;

        $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
            'utilisateur' => $utilisateur,
            'paye' => true
        ]);

        $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
            'utilisateur' => $utilisateur,
            'paye' => true
        ]);

        $versements = $em->getRepository('AppBundle:Versement')->findBy([
            'utilisateur' => $utilisateur,
            'statut' => true
        ]);

        foreach ($collectes as $collecte) {
            $montantCollecte += $collecte->getMontant();
        }

        foreach ($adhesions as $adhesion) {
            $montantAdhesion += $adhesion->getMontant();
        }

        foreach ($versements as $versement) {
            $ancienRetrait += $versement->getMontant();
        }

        return $montantCollecte + $montantAdhesion - $ancienRetrait ;
    }

    public function buildSoldeCampagne(Utilisateur $utilisateur, $campagne)
    {
        $em = $this->getDoctrine()->getManager();

        $montantCollecte = 0;
        $montantAdhesion = 0;
        $ancienRetrait = 0;

        if ($campagne->getTypeCampagne() == "COLLECTE") {
            $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
                'campagne' => $campagne,
                'paye' => true
            ]);

            foreach ($collectes as $collecte) {
                $montantCollecte += $collecte->getMontant();
            }

        }elseif($campagne->getTypeCampagne() == "ADHESION"){
            $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
                'campagne' => $campagne,
                'paye' => true
            ]);

            foreach ($adhesions as $adhesion) {
                $montantAdhesion += $adhesion->getMontant();
            }
        }

        $versements = $em->getRepository('AppBundle:Versement')->findBy([
            'statut' => true,
            'campagne' => $campagne
        ]);


        foreach ($versements as $versement) {
            $ancienRetrait += $versement->getMontant();
        }

        return $montantCollecte + $montantAdhesion - $ancienRetrait ;
    }

    public function findAmountCampagne($campagne)
    {
        $em = $this->getDoctrine()->getManager();

        $montantCollecte = 0;
        $montantAdhesion = 0;

        if ($campagne->getTypeCampagne() == "COLLECTE") {
            $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
                'campagne' => $campagne,
                'paye' => true
            ]);

            foreach ($collectes as $collecte) {
                $montantCollecte += $collecte->getMontant();
            }

        }elseif($campagne->getTypeCampagne() == "ADHESION"){
            $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
                'campagne' => $campagne,
                'paye' => true
            ]);

            foreach ($adhesions as $adhesion) {
                $montantAdhesion += $adhesion->getMontant();
            }
        }

        return $montantCollecte + $montantAdhesion;
    }

}