<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Application controller.
 *
 * @Route("manage/application")
 */
class ApplicationController extends FunctionController
{
    /**
     * Lists all application entities.
     *
     * @Route("/", name="application_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($this->getUser() && $this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN'){
            $applications = $em->getRepository('AppBundle:Application')->findBy([
                'utilisateur' => $this->getUser()
            ]);
        }else{
            $applications = $em->getRepository('AppBundle:Application')->findAll();
        }

        return $this->render('application/index.html.twig', array(
            'applications' => $applications,
        ));
    }

    /**
     * Lists all application for a user entities.
     *
     * @Route("/app-for-current-user", name="application_for_user")
     * @Method("GET")
     */
    public function appUserAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($this->getUser() && $this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN'){
            $applications = $em->getRepository('AppBundle:Application')->findBy([
                'utilisateur' => $this->getUser()
            ]);
        }else{
            $applications = $em->getRepository('AppBundle:Application')->findAll();
        }

        return $this->render('application/app-user.html.twig', array(
            'applications' => $applications,
        ));
    }

    /**
     * Creates a new application entity.
     *
     * @Route("/new", name="application_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $application = new Application();
        $form = $this->createForm('AppBundle\Form\ApplicationType', $application);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            //Importation du logo
            if(!empty($_FILES['appbundle_application']['name']['logo'])){
                $fichier = '';
                $fichier = $this->SingleImportation($fichier, 'logos');
                $application->setLogo($fichier);
            }
            $dateCreation = new \DateTime();
            $application->setDateCreation($dateCreation);
            $application->setUtilisateur($this->getUser());

            $em->persist($application);
            $em->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Association/Application ajoutée avec succès');

            return $this->redirectToRoute('application_show', array('id' => $application->getId()));
        }

        return $this->render('application/new.html.twig', array(
            'application' => $application,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a application entity.
     *
     * @Route("/{id}", name="application_show")
     * @Method("GET")
     */
    public function showAction(Application $application)
    {
        $deleteForm = $this->createDeleteForm($application);

        $em = $this->getDoctrine()->getManager();

        $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
            'application' => $application,
            'paye' => true
        ]);

        $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
            'application' => $application,
            'paye' => true
        ]);

        $montantCollecte = 0;
        foreach ($collectes as $collecte) {
            $montantCollecte += $collecte->getMontant();
        }

        return $this->render('application/show.html.twig', array(
            'application' => $application,
            'delete_form' => $deleteForm->createView(),
            'collectes' => $collectes,
            'adhesions' => $adhesions,
            'montantCollecte' => $montantCollecte
        ));
    }


    /**
     * Displays a form to edit an existing application entity.
     *
     * @Route("/{id}/edit", name="application_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Application $application)
    {
        $deleteForm = $this->createDeleteForm($application);

        
        $file = $application->getLogo();
        $application->setLogo(NULL);
        $editForm = $this->createForm('AppBundle\Form\ApplicationType', $application);
        $editForm->handleRequest($request);

        $application->setLogo($file);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            //Importation du logo
            if(!empty($_FILES['appbundle_application']['name']['logo'])){

                //Suppression de l'ancien logo
                $dossier = $this->get('kernel')->getRootDir() . '/../web/Uploads/logos/';
                if(file_exists($dossier.$application->getLogo())) {
                    unlink($dossier.$application->getLogo());
                } else{
                    //RAS
                }

                //Upload du nouveau
                $fichier = '';
                $fichier = $this->SingleImportation($fichier, 'logos');
                $application->setLogo($fichier);
            }
            $this->getDoctrine()->getManager()->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Association/Application mise à jour avec succès');

            return $this->redirectToRoute('application_edit', array('id' => $application->getId()));
        }

        return $this->render('application/edit.html.twig', array(
            'application' => $application,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a application entity.
     *
     * @Route("/{id}", name="application_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Application $application)
    {
        $form = $this->createDeleteForm($application);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($application);
            $em->flush();
        }

        return $this->redirectToRoute('application_index');
    }

    /**
     * Creates a form to delete a application entity.
     *
     * @param Application $application The application entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Application $application)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('application_delete', array('id' => $application->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
