<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Adhesion;
use AppBundle\Entity\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Adhesion controller.
 *
 * @Route("manage/adhesion")
 */
class AdhesionController extends FunctionController
{
    /**
     * Lists all adhesion entities.
     *
     * @Route("/", name="adhesion_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
            'paye' => true
        ]);

        return $this->render('adhesion/index.html.twig', array(
            'adhesions' => $adhesions,
            'applications' => $this->findApplications()
        ));
    }


    /**
     * Lists all adhesion pour un application entities.
     *
     * @Route("/pour-application/KJGGI177863{id}8711", name="adhesion_application_index")
     * @Method("GET")
     */
    public function adhesionAppliAction(Application $application)
    {
        $em = $this->getDoctrine()->getManager();

        $adhesions = $em->getRepository('AppBundle:Adhesion')->findBy([
            'application' =>$application,
            'paye' => true
        ]);

        return $this->render('adhesion/index.html.twig', array(
            'adhesions' => $adhesions,
            'application' => $application,
            'applications' => $this->findApplications()
        ));
    }

    /**
     * Creates a new adhesion entity.
     *
     * @Route("/new", name="adhesion_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $adhesion = new Adhesion();
        $adhesion->setDateAdhesion(new \DateTime());
        $adhesion->setPaye(true);
        $adhesion->setPaye(true);
        $form = $this->createForm('AppBundle\Form\AdhesionType', $adhesion);
        $form->add('application');
        $form->add('campagne');
        $form->add('montant');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $adhesion->setUtilisateur($adhesion->getCampagne()->getUtilisateur());


            $em->persist($adhesion);
            $em->flush();

            return $this->redirectToRoute('adhesion_index');
        }

        return $this->render('adhesion/new.html.twig', array(
            'adhesion' => $adhesion,
            'form' => $form->createView(),
            'applications' => $this->findApplications()
        ));
    }



    /**
     * Inégration du formulaire d'adhesion.
     *
     * @Route("/campagne-de-/{campagne}/{application}/formulaire/id=HDKHF7010821{id}897GTSPH", name="adhesion_form")
     * @Method({"GET", "POST"})
     */
    public function adhesionFormAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $campagne = $em->getRepository('AppBundle:Campagne')->find($id);

        if($campagne && !$campagne->getFermer()){
            $adhesion = new Adhesion();

            //customisation de la adhesion
            $adhesion->setCampagne($campagne);
            $adhesion->setapplication($campagne->getApplication());
            $adhesion->setDateAdhesion(new \DateTime());

            //Assignation du  formulaire
            $form = $this->createForm('AppBundle\Form\AdhesionType', $adhesion);
            $form->remove('dateAdhesion');
            $form->remove('campagne');
            $form->remove('application');
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $adhesion->setMontant($campagne->getMontant());
                $adhesion->setPaye(false);
                $adhesion->setSession($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);

                $adhesion->setUtilisateur($adhesion->getCampagne()->getUtilisateur());
                
                $em->persist($adhesion);
                $em->flush();

                if($campagne->getPayante()){
                    return $this->redirectToRoute('adhesion_pay', array(
                        'id' => $adhesion->getId(),
                        'campagne' => $campagne,
                        'application' => $campagne->getApplication()
                    ));
                }else{
                    $adhesion->setPaye(true);
                    $em->flush();
                    return $this->redirectToRoute('adhesion_index');
                }
            }

            return $this->render('adhesion/adhesion.html.twig', array(
                'adhesion' => $adhesion,
                'campagne' => $campagne,
                'form' => $form->createView(),
            ));
        }else{
            return $this->render('campagne/lien-mort.html.twig');
        }
    }


    /**
     * Inégration du formulaire de adhesion.
     *
     * @Route("/pay/campagne-de-{campagne}/{application}/formulaire/id=HDKHF701082{id}897GTSH", name="adhesion_pay")
     * @Method({"GET", "POST"})
     */
    public function collectePayAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $adhesion = $em->getRepository('AppBundle:Adhesion')->find($id);
        if(!$adhesion){
            return $this->render('campagne/lien-mort.html.twig');
        }
        $elements = $request->request->all();

        if(isset($elements['choixMethod'])){
            $choix = $elements['moyen'];

            if($choix == "1"){
                //Vue PayPal
                return $this->render('partial/paiement/paypal.html.twig', array(
                    'adhesion' => $adhesion 
                ));
            }
            if($choix == "2"){
                //Vue Carte payPlug

            }
            if($choix == "3"){
                //Vue Stripe
                return $this->render('partial/paiement/stripe.html.twig', array(
                    'adhesion' => $adhesion,
                ));
            }
        }

        return $this->render('adhesion/choixPayMethod.html.twig', array(
            'adhesion' => $adhesion,
            'CHECKOUT_SESSION_ID' => $this->builPayStripeId($adhesion->getMontant(), $this->getUser(), null, $adhesion, 'ADHESION')
        ));
    }


    /**
     * Finds and displays a adhesion entity.
     *
     * @Route("/{id}", name="adhesion_show")
     * @Method("GET")
     */
    public function showAction(Adhesion $adhesion)
    {
        $deleteForm = $this->createDeleteForm($adhesion);

        return $this->render('adhesion/show.html.twig', array(
            'adhesion' => $adhesion,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing adhesion entity.
     *
     * @Route("/{id}/edit", name="adhesion_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Adhesion $adhesion)
    {
        $deleteForm = $this->createDeleteForm($adhesion);
        $editForm = $this->createForm('AppBundle\Form\AdhesionType', $adhesion);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('adhesion_edit', array('id' => $adhesion->getId()));
        }

        return $this->render('adhesion/edit.html.twig', array(
            'adhesion' => $adhesion,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a adhesion entity.
     *
     * @Route("/{id}", name="adhesion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Adhesion $adhesion)
    {
        $form = $this->createDeleteForm($adhesion);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($adhesion);
            $em->flush();
        }

        return $this->redirectToRoute('adhesion_index');
    }

    /**
     * Creates a form to delete a adhesion entity.
     *
     * @param Adhesion $adhesion The adhesion entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Adhesion $adhesion)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('adhesion_delete', array('id' => $adhesion->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
