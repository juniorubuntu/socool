<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Campagne;
use AppBundle\Entity\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Campagne controller.
 *
 * @Route("manage/campagne")
 */
class CampagneController extends FunctionController
{
    /**
     * Lists all campagne entities.
     *
     * @Route("/", name="campagne_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($this->getUser() && $this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN'){
            $campagnes = $em->getRepository('AppBundle:Campagne')->findBy([
                'utilisateur' => $this->getUser()
            ]);
        }else{
            $campagnes = $em->getRepository('AppBundle:Campagne')->findAll();
        }

        return $this->render('campagne/index.html.twig', array(
            'campagnes' => $campagnes,
            'applications' => $this->findApplications()
        ));
    }


    /**
     * Lists all campagnes entities for application.
     *
     * @Route("/creer/672HJGD23{id}872", name="campagne_appli")
     * @Method("GET")
     */
    public function CampagneAppliAction(Application $application)
    {
        $em = $this->getDoctrine()->getManager();

        if($this->getUser() && $this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN'){
            $campagnes = $em->getRepository('AppBundle:Campagne')->findBy([
                'utilisateur' => $this->getUser(),
                'application' => $application
            ]);
        }else{
            $campagnes = $em->getRepository('AppBundle:Campagne')->findAll();
        }

        return $this->render('campagne/index.html.twig', array(
            'campagnes' => $campagnes,
            'application' => $application,
            'applications' => $this->findApplications()
        ));
    }

    /**
     * Creates a new campagne entity.
     *
     * @Route("/new", name="campagne_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $campagne = new Campagne();
        $campagne->setUtilisateur($this->getUser());
        $form = $this->createForm('AppBundle\Form\CampagneType', $campagne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (!$campagne->getIllimite() && ($campagne->getDateDebut() && $campagne->getDateFin())) {

                //Importation de l'image de couverture
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $em->persist($campagne);
                $em->flush();

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne crée avec succès</h3>Commencez le paramétrage de votre campagne dès maintenant en lui associant un formulaire');
                return $this->redirectToRoute('campagne_show', array(
                    'id' => $campagne->getId(),
                    'applications' => $this->findApplications()
                ));

            }elseif($campagne->getIllimite()){
                $campagne->setDateDebut(null);
                $campagne->setDateFin(null);

                //Importation de l'image de couverture
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $em->persist($campagne);
                $em->flush();

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne crée avec succès</h3>Commencez le paramétrage de votre campagne dès maintenant en lui associant un formulaire');
                return $this->redirectToRoute('campagne_show', array(
                    'id' => $campagne->getId(),
                    'applications' => $this->findApplications()
                ));
            }else{

                //On ne fait rien
                $request->getSession()
                ->getFlashBag()
                ->add('echec', '<h3>Une ou plusieurs informations sur le fomulaire sont incorrectes</h3>Si votre campagne est limitée, préciser la date de début et date de fin');
            }
        }

        return $this->render('campagne/new.html.twig', array(
            'campagne' => $campagne,
            'form' => $form->createView(),
            'applications' => $this->findApplications()
        ));
    }

    /**
     * Creates a new campagne COLLECTE entity.
     *
     * @Route("/collecte/{id}/new", name="campagne_collecte_new")
     * @Method({"GET", "POST"})
     */
    public function newCampagneCollecteAction(Request $request, Application $application = null)
    {

        $campagne = new Campagne();

        //On gère le cas des campagnes pour application directement
        $user = $this->getUser();
        if($application){
            if(($user->getLevel()->getRightToken() == 'ROLE_ADMIN') || ($application->getUtilisateur() == $user)){
                $campagne->setApplication($application);
            }
        }
        $campagne->setTypeCampagne("COLLECTE");
        $campagne->setUtilisateur($this->getUser());
        $form = $this->createForm('AppBundle\Form\CampagneType', $campagne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (!$campagne->getIllimite() && ($campagne->getDateDebut() && $campagne->getDateFin())) {

                //Importation de l'image de couverture
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $em->persist($campagne);
                $em->flush();

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne crée avec succès</h3>Commencez le paramétrage de votre campagne dès maintenant en lui associant un formulaire');
                return $this->redirectToRoute('campagne_show', array('id' => $campagne->getId()));
                
            }elseif($campagne->getIllimite()){
                $campagne->setDateDebut(null);
                $campagne->setDateFin(null);

                //Importation de l'image de couverture
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $em->persist($campagne);
                $em->flush();

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne crée avec succès</h3>Commencez le paramétrage de votre campagne dès maintenant en lui associant un formulaire');
                return $this->redirectToRoute('campagne_show', array(
                    'id' => $campagne->getId(),
                    'applications' => $this->findApplications()
                ));
            }else{
                //On ne fait rien
                $request->getSession()
                ->getFlashBag()
                ->add('echec', '<h3>Une ou plusieurs informations sur le fomulaire sont incorrectes</h3>Si votre campagne est limitée, préciser la date de début et date de fin');
            }
        }

        return $this->render('campagne/new.html.twig', array(
            'campagne' => $campagne,
            'application' => $application,
            'form' => $form->createView(),
            'applications' => $this->findApplications()
        ));
    }


    /**
     * Creates a new campagne COLLECTE entity.
     *
     * @Route("/adhesion/{id}/new", name="campagne_adhesion_new")
     * @Method({"GET", "POST"})
     */
    public function newCampagneAdhesionAction(Request $request, Application $application = null)
    {

        $campagne = new Campagne();

        //On gère le cas des campagnes pour application directement
        $user = $this->getUser();
        if($application){
            if(($user->getLevel()->getRightToken() == 'ROLE_ADMIN') || ($application->getUtilisateur() == $user)){
                $campagne->setApplication($application);
            }
        }

        $campagne->setTypeCampagne("ADHESION");
        $campagne->setUtilisateur($this->getUser());
        $form = $this->createForm('AppBundle\Form\CampagneType', $campagne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if (!$campagne->getIllimite() && ($campagne->getDateDebut() && $campagne->getDateFin())) {

                //Importation de l'image de couverture
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $em->persist($campagne);
                $em->flush();

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne crée avec succès</h3>Commencez le paramétrage de votre campagne dès maintenant en lui associant un formulaire');
                return $this->redirectToRoute('campagne_show', array('id' => $campagne->getId()));
                
            }elseif($campagne->getIllimite()){
                $campagne->setDateDebut(null);
                $campagne->setDateFin(null);

                //Importation de l'image de couverture
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $em->persist($campagne);
                $em->flush();

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne crée avec succès</h3>Commencez le paramétrage de votre campagne dès maintenant en lui associant un formulaire');
                return $this->redirectToRoute('campagne_show', array(
                    'id' => $campagne->getId(),
                    'applications' => $this->findApplications()
                ));
            }else{
                //On ne fait rien
                $request->getSession()
                ->getFlashBag()
                ->add('echec', '<h3>Une ou plusieurs informations sur le fomulaire sont incorrectes</h3>Si votre campagne est limitée, préciser la date de début et date de fin');
            }
        }

        return $this->render('campagne/new.html.twig', array(
            'campagne' => $campagne,
            'application' => $application,
            'form' => $form->createView(),
            'applications' => $this->findApplications()
        ));
    }

    /**
     * Finds and displays a campagne entity.
     *
     * @Route("/{id}", name="campagne_show")
     * @Method("GET")
     */
    public function showAction(Campagne $campagne)
    {
        $em = $this->getDoctrine()->getManager();
        if ($campagne->getTypeCampagne() == "COLLECTE"){
            $participations = $em->getRepository('AppBundle:Collecte')->findBy([
                'campagne' => $campagne,
                'paye' =>true
            ]);
        }elseif ($campagne->getTypeCampagne() == "ADHESION") {
            $participations = $em->getRepository('AppBundle:Adhesion')->findBy([
                'campagne' => $campagne,
                'paye' =>true
            ]);
        }

        return $this->render('campagne/show.html.twig', array(
            'campagne' => $campagne,
            'application' => $campagne->getApplication(),
            'applications' => $this->findApplications(),
            'montantCollecte' => $this->findAmountCampagne($campagne),
            'participations' => $participations,
            'soldeRestant' => $this->buildSoldeCampagne($campagne->getUtilisateur(), $campagne)
        ));
    }

    /**
     * Manage campagne.
     *
     * @Route("/gerer-la-campagne/{id}", name="campagne_manage")
     * @Method({"GET", "POST"})
     */
    public function manageCampagneAction(Campagne $campagne)
    {
        
        $user = $this->getUser();
        if(($campagne->getUtilisateur() != $user) && ($this->getUser()->getLevel()->getRightToken() != 'ROLE_ADMIN')){
            return $this->redirectToRoute('homepage');
        }

        $em = $this->getDoctrine()->getManager();

        if ($campagne->getTypeCampagne() == "COLLECTE"){
            $participations = $em->getRepository('AppBundle:Collecte')->findBy([
                'campagne' => $campagne,
                'paye' =>true
            ]);
        }

        if ($campagne->getTypeCampagne() == "ADHESION") {
            $participations = $em->getRepository('AppBundle:Adhesion')->findBy([
                'campagne' => $campagne,
                'paye' =>true
            ]);
        }

        return $this->render('campagne/show.html.twig', array(
            'campagne' => $campagne,
            'application' => $campagne->getApplication(),
            'applications' => $this->findApplications(),
            'montantCollecte' => $this->findAmountCampagne($campagne),
            'participations' => $participations,
            'soldeRestant' => $this->buildSoldeCampagne($campagne->getUtilisateur(), $campagne)
        ));
    }

    /**
     * Displays a form to edit an existing campagne entity.
     *
     * @Route("/{id}/edit", name="campagne_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Campagne $campagne)
    {
        $deleteForm = $this->createDeleteForm($campagne);

        $file = $campagne->getImage();
        $campagne->setImage(NULL);

        $editForm = $this->createForm('AppBundle\Form\CampagneType', $campagne);
        $editForm->remove('application');
        $editForm->handleRequest($request);

        $campagne->setImage($file);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if (!$campagne->getIllimite() && (!$campagne->getDateDebut() || !$campagne->getDateFin())) {

                //On ne fait rien
                $request->getSession()
                ->getFlashBag()
                ->add('echec', '<h3>Une ou plusieurs informations sur le fomulaire sont incorrectes</h3>Si votre campagne est limitée, préciser la date de début et date de fin');
            }else{

                if($campagne->getIllimite() || !$campagne->getDateDebut() || !$campagne->getDateFin()){
                    $campagne->setDateDebut(null);
                    $campagne->setDateFin(null);
                }

                if($campagne->getTypeCampagne() == "ADHESION"){
                    $campagne->setCategorie(null);
                }

                $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne mise à jour avec succès</h3><a class="" href="'.$this->generateUrl("campagne_show", ["id" => $campagne->getId()]).'">Voir la campagne</a> / <a class="" href="'.$this->generateUrl("campagne_index").'">Retourner à la liste</a>');

                //Importation du logo
                if(!empty($_FILES['appbundle_campagne']['name']['image'])){

                    //Suppression de l'ancienne image
                    $dossier = $this->get('kernel')->getRootDir() . '/../web/Uploads/couvertures/';
                    if(file_exists($dossier.$campagne->getImage()) && $file) {
                        unlink($dossier.$campagne->getImage());
                    } else{
                        //RAS
                    }

                    //Upload du nouveau
                    $fichier = '';
                    $fichier = $this->SingleImportationCouverture($fichier, 'couvertures');
                    $campagne->setImage($fichier);
                }

                $this->getDoctrine()->getManager()->flush();
                
                return $this->redirectToRoute('campagne_edit', array('id' => $campagne->getId()));
            }
        }

        return $this->render('campagne/edit.html.twig', array(
            'campagne' => $campagne,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit' => true
        ));
    }


    /**
     * Displays a form to edit an existing campagne entity.
     *
     * @Route("/gerer/{id}/close", name="campagne_close")
     * @Method({"GET", "POST"})
     */
    public function closeAction(Request $request, Campagne $campagne)
    {
        $campagne->setFermer(true);
        $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne clôturée avec succès</h3><a class="" href="'.$this->generateUrl("campagne_show", ["id" => $campagne->getId()]).'">Voir la campagne</a> / <a class="" href="'.$this->generateUrl("campagne_index").'">Retourner à la liste</a>');

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('campagne_edit', array('id' => $campagne->getId()));
    }

    /**
     * Displays a form to edit an existing campagne entity.
     *
     * @Route("/gerer/{id}/open", name="campagne_open")
     * @Method({"GET", "POST"})
     */
    public function openAction(Request $request, Campagne $campagne)
    {
        $campagne->setFermer(false);
        $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Campagne relanceé avec succès</h3><a class="" href="'.$this->generateUrl("campagne_show", ["id" => $campagne->getId()]).'">Voir la campagne</a> / <a class="" href="'.$this->generateUrl("campagne_index").'">Retourner à la liste</a>');
                
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('campagne_edit', array('id' => $campagne->getId()));
    }


    /**
     * Deletes a campagne entity.
     *
     * @Route("/{id}", name="campagne_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Campagne $campagne)
    {
        $form = $this->createDeleteForm($campagne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($campagne);
            $em->flush();
        }

        return $this->redirectToRoute('campagne_index');
    }

    /**
     * Creates a form to delete a campagne entity.
     *
     * @param Campagne $campagne The campagne entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Campagne $campagne)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('campagne_delete', array('id' => $campagne->getId())))
        ->setMethod('DELETE')
        ->getForm()
        ;
    }
}
