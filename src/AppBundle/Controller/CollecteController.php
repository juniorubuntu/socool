<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Collecte;
use AppBundle\Entity\Application;
use AppBundle\Entity\Campagne;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Collecte controller.
 *
 * @Route("manage/collecte")
 */
class CollecteController extends FunctionController
{
    /**
     * Lists all collecte entities.
     *
     * @Route("/", name="collecte_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
            'paye' => true
        ]);

        return $this->render('collecte/index.html.twig', array(
            'collectes' => $collectes,
            'applications' => $this->findApplications()
        ));
    }


    /**
     * Lists all collecte entities.
     *
     * @Route("/pour-application/KJGGI177863{id}8711", name="collecte_application_index")
     * @Method("GET")
     */
    public function collecteAppliAction(Application $application)
    {
        $em = $this->getDoctrine()->getManager();

        $collectes = $em->getRepository('AppBundle:Collecte')->findBy([
            'application' =>$application,
            'paye' => true
        ]);

        return $this->render('collecte/index.html.twig', array(
            'collectes' => $collectes,
            'application' => $application,
            'applications' => $this->findApplications()
        ));
    }

    /**
     * Creates a new collecte entity.
     *
     * @Route("/new", name="collecte_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $collecte = new Collecte();
        $collecte->setDateCollecte(new \DateTime());
        $collecte->setPaye(true);
        $form = $this->createForm('AppBundle\Form\CollecteType', $collecte);
        $form->add('application');
        $form->add('campagne');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $collecte->setUtilisateur($collecte->getCampagne()->getUtilisateur());

            $em->persist($collecte);
            $em->flush();

            return $this->redirectToRoute('collecte_index');
        }

        return $this->render('collecte/new.html.twig', array(
            'collecte' => $collecte,
            'form' => $form->createView(),
            'applications' => $this->findApplications()
        ));
    }


    /**
     * Inégration du formulaire de collecte.
     *
     * @Route("/campagne-de-/{campagne}/{application}/formulaire/id=HDKHF701082{id}897GTSH", name="collecte_form")
     * @Method({"GET", "POST"})
     */
    public function collecteFormAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $campagne = $em->getRepository('AppBundle:Campagne')->find($id);

        if($campagne && !$campagne->getFermer()){
            $collecte = new Collecte();

            //customisation de la collecte
            $collecte->setCampagne($campagne);
            $collecte->setapplication($campagne->getApplication());
            $collecte->setDateCollecte(new \DateTime());

            //Assignation du  formulaire
            $form = $this->createForm('AppBundle\Form\CollecteType', $collecte);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $collecte->setPaye(false);
                $collecte->setSession($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
                
                $collecte->setUtilisateur($collecte->getCampagne()->getUtilisateur());

                $em->persist($collecte);
                $em->flush();

                return $this->redirectToRoute('collecte_pay', array(
                    'id' => $collecte->getId(),
                    'campagne' => $campagne,
                    'application' => $campagne->getApplication()
                ));
            }

            return $this->render('collecte/collecte.html.twig', array(
                'collecte' => $collecte,
                'campagne' => $campagne,
                'form' => $form->createView(),
            ));
        }else{
            return $this->render('campagne/lien-mort.html.twig');
        }
    }

    /**
     * Inégration du formulaire de collecte.
     *
     * @Route("/pay/campagne-de-{campagne}/{application}/formulaire/id=HDKHF701082{id}897GTSH", name="collecte_pay")
     * @Method({"GET", "POST"})
     */
    public function collectePayAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $collecte = $em->getRepository('AppBundle:Collecte')->find($id);
        if(!$collecte){
            return $this->render('campagne/lien-mort.html.twig');
        }
        $elements = $request->request->all();

        if(isset($elements['choixMethod'])){
            $choix = $elements['moyen'];

            if($choix == "1"){
                //Vue PayPal
                return $this->render('partial/paiement/paypal.html.twig', array(
                    'transfert' => $transfert 
                ));
            }
            if($choix == "2"){
                //Vue Carte payPlug

            }
            if($choix == "3"){
                //Vue Stripe
                return $this->render('partial/paiement/stripe.html.twig', array(
                    'collecte' => $collecte,
                ));
            }
        }

        return $this->render('collecte/choixPayMethod.html.twig', array(
            'collecte' => $collecte,
            'CHECKOUT_SESSION_ID' => $this->builPayStripeId($collecte->getMontant(), $this->getUser(), $collecte, null, 'COLLECTE')
        ));
    }

    /**
     * Finds and displays a collecte entity.
     *
     * @Route("/{id}", name="collecte_show")
     * @Method("GET")
     */
    public function showAction(Collecte $collecte)
    {
        $deleteForm = $this->createDeleteForm($collecte);

        return $this->render('collecte/show.html.twig', array(
            'collecte' => $collecte,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing collecte entity.
     *
     * @Route("/{id}/edit", name="collecte_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Collecte $collecte)
    {
        $deleteForm = $this->createDeleteForm($collecte);
        $editForm = $this->createForm('AppBundle\Form\CollecteType', $collecte);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('collecte_edit', array('id' => $collecte->getId()));
        }

        return $this->render('collecte/edit.html.twig', array(
            'collecte' => $collecte,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a collecte entity.
     *
     * @Route("/{id}", name="collecte_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Collecte $collecte)
    {
        $form = $this->createDeleteForm($collecte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($collecte);
            $em->flush();
        }

        return $this->redirectToRoute('collecte_index');
    }

    /**
     * Creates a form to delete a collecte entity.
     *
     * @param Collecte $collecte The collecte entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Collecte $collecte)
    {
        return $this->createFormBuilder()
        ->setAction($this->generateUrl('collecte_delete', array('id' => $collecte->getId())))
        ->setMethod('DELETE')
        ->getForm()
        ;
    }
}
