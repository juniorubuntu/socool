<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Versement;
use AppBundle\Entity\Campagne;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Versement controller.
 *
 * @Route("manage/versement")
 */
class VersementController extends FunctionController
{
    /**
     * Lists all versement entities.
     *
     * @Route("/", name="versement_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $versements = $em->getRepository('AppBundle:Versement')->findAll();

        return $this->render('versement/index.html.twig', array(
            'versements' => $versements,
            'applications' => $this->findApplications()
        ));
    }

    /**
     * Creates a new versement entity.
     *
     * @Route("/new", name="versement_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $versement = new Versement();
        $versement->setDateDemande(new \Datetime());
        $versement->setUtilisateur($this->getUser());
        $form = $this->createForm('AppBundle\Form\VersementType', $versement);
        $form->handleRequest($request);
        $solde = $this->buildSolde($this->getUser());
        $frais = $em->getRepository('AppBundle:Frais')->findOneBy([]);

        if ($form->isSubmitted() && $form->isValid()) {

            if(is_numeric($versement->getMontant())){
                if($versement->getMontant() <= $solde && $versement->getMontant() <= $frais->getMaximum() && $versement->
                    getMontant() >= $frais->getMinimum()){

                    //Prélevement des commissions
                    $retenue = $versement->getMontant() * ($frais->getPourcentage() / 100);
                    $percevoir = $versement->getMontant() - $retenue;

                    //Mise à jour de la demande de versement
                    $versement->setRetenue($retenue);
                    $versement->setPercevoir($percevoir);
                    $versement->setFrais($frais->getPourcentage());

                    $em->persist($versement);
                    $em->flush();

                    $request->getSession()
                    ->getFlashBag()
                    ->add('success', '<h3>Demande initée avec succès</h3>Vous verez ci-dessous les informations liées à son traitement.');

                    return $this->redirectToRoute('versement_index');
                }else{
                    $request->getSession()
                    ->getFlashBag()
                    ->add('echec', '<h3>Problème lors de création de la demande</h3>Vérifiez les informations du formulaire et reessayer.');
                }
            }else{
                $request->getSession()
                    ->getFlashBag()
                    ->add('echec', '<h3>Problème lors de création de la demande</h3>Vérifiez les informations du formulaire et reessayer.');
            }
        }

        return $this->render('versement/new.html.twig', array(
            'versement' => $versement,
            'form' => $form->createView(),
            'applications' => $this->findApplications(),
            'solde' => $solde,
            'frais' => $frais
        ));
    }

    /**
     * Cherche la valeur à retenir lors d'un retrait.
     *
     * @Route("/build-retenue/{montant}/{solde}", name="build_retenue")
     * @Method({"GET", "POST"})
     */
    public function build_retenueAction(Request $request, $montant, $solde)
    {
        $em = $this->getDoctrine()->getManager();
        $frais = $em->getRepository('AppBundle:Frais')->findOneBy([]);
        $percevoir = 0;

        if(is_numeric($montant)){
            if($montant <= $solde && $montant <= $frais->getMaximum() && $montant >= $frais->getMinimum()){
                $retenue = $montant * ($frais->getPourcentage() / 100);
                $percevoir = $montant - $retenue;
            }else{
                $retenue = "error";
            }
        }else{
            $retenue = "error";
        }

        return $this->render('versement/submitDemande.html.twig', [
            'retenue' => $retenue,
            'percevoir' => $percevoir,
            'frais' => $em->getRepository('AppBundle:Frais')->findOneBy([])
        ]);
    }



    /**
     * Finds and displays a versement entity.
     *
     * @Route("/{id}", name="versement_show")
     * @Method("GET")
     */
    public function showAction(Versement $versement)
    {
        $deleteForm = $this->createDeleteForm($versement);

        return $this->render('versement/show.html.twig', array(
            'versement' => $versement,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Finds and displays a versement entity.
     *
     * @Route("/admin/solve/{id}", name="versement_solve")
     * @Method({"GET", "POST"})
     */
    public function solveAction(Versement $versement, Request $request)
    {
        $versement->setStatut(true);
        $this->getDoctrine()->getManager()->flush();

        $request->getSession()
        ->getFlashBag()
        ->add('success', '<h3>Demande traitée avec succès</h3>Un mail a été envoyé au demandeur pour l\'en informé du traitement.');

        return $this->redirectToRoute('versement_index');
    }

    /**
     * comment a versement
     *
     * @Route("/{id}/admin/comment", name="versement_comment")
     * @Method({"GET", "POST"})
     */
    public function commentAction(Versement $versement, Request $request)
    {
        $editForm = $this->createForm('AppBundle\Form\VersementType', $versement);
        $editForm->add('commentaire');
        $editForm->remove('montant');
        $editForm->remove('percevoir');
        $editForm->remove('retenue');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $request->getSession()
            ->getFlashBag()
            ->add('success', '<h3>Commentaire envoyé avec succès</h3>Un mail a été envoyé au demandeur pour l\'en informé de la réponse.');
            return $this->redirectToRoute('versement_index');
        }
        return $this->render('versement/comment.html.twig', array(
            'versement' => $versement,
            'form' => $editForm->createView()
        ));
    }

    /**
     * Displays a form to edit an existing versement entity.
     *
     * @Route("/{id}/edit", name="versement_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Versement $versement)
    {
        $deleteForm = $this->createDeleteForm($versement);
        $editForm = $this->createForm('AppBundle\Form\VersementType', $versement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('versement_edit', array('id' => $versement->getId()));
        }

        return $this->render('versement/edit.html.twig', array(
            'versement' => $versement,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a versement entity.
     *
     * @Route("/{id}", name="versement_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Versement $versement)
    {
        $form = $this->createDeleteForm($versement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$em->remove($versement);
            $em->flush();
        }

        return $this->redirectToRoute('versement_index');
    }

    /**
     * Creates a form to delete a versement entity.
     *
     * @param Versement $versement The versement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Versement $versement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('versement_delete', array('id' => $versement->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * Creates a new versement entity for a campaign.
     *
     * @Route("/nouveau-versement-pour-campagne/SHDHJ652{id}1XHC5JKA", name="versement_campagne")
     * @Method({"GET", "POST"})
     */
    public function newVersCampagneAction(Request $request, Campagne $campagne)
    {
        $em = $this->getDoctrine()->getManager();
        $versement = new Versement();
        $versement->setCampagne($campagne);
        $versement->setDateDemande(new \Datetime());
        $versement->setUtilisateur($this->getUser());
        $form = $this->createForm('AppBundle\Form\VersementType', $versement);
        $form->handleRequest($request);
        $solde = $this->buildSoldeCampagne($this->getUser());
        $frais = $em->getRepository('AppBundle:Frais')->findOneBy([]);

        if ($form->isSubmitted() && $form->isValid()) {

            if(is_numeric($versement->getMontant())){
                if($versement->getMontant() <= $solde && $versement->getMontant() <= $frais->getMaximum() && $versement->
                    getMontant() >= $frais->getMinimum()){

                    //Prélevement des commissions
                    $retenue = $versement->getMontant() * ($frais->getPourcentage() / 100);
                    $percevoir = $versement->getMontant() - $retenue;

                    //Mise à jour de la demande de versement
                    $versement->setRetenue($retenue);
                    $versement->setPercevoir($percevoir);
                    $versement->setFrais($frais->getPourcentage());

                    $em->persist($versement);
                    $em->flush();

                    $request->getSession()
                    ->getFlashBag()
                    ->add('success', '<h3>Demande initée avec succès</h3>Vous verez ci-dessous les informations liées à son traitement.');

                    return $this->redirectToRoute('versement_index');
                }else{
                    $request->getSession()
                    ->getFlashBag()
                    ->add('echec', '<h3>Problème lors de création de la demande</h3>Vérifiez les informations du formulaire et reessayer.');
                }
            }else{
                $request->getSession()
                    ->getFlashBag()
                    ->add('echec', '<h3>Problème lors de création de la demande</h3>Vérifiez les informations du formulaire et reessayer.');
            }
        }

        return $this->render('versement/new.html.twig', array(
            'versement' => $versement,
            'form' => $form->createView(),
            'applications' => $this->findApplications(),
            'solde' => $solde,
            'frais' => $frais
        ));
    }

}
