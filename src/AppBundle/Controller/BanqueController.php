<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Banque;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Banque controller.
 *
 * @Route("manage/banque")
 */
class BanqueController extends FunctionController
{
    /**
     * Lists all banque entities.
     *
     * @Route("/", name="banque_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($this->getUser()->getLevel()->getRightToken() == "ROLE_ADMIN"){
            $banques = $em->getRepository('AppBundle:Banque')->findAll();

        }else{
            $banques = $em->getRepository('AppBundle:Banque')->findOneBy([
                'utilisateur' => $this->getUser()
            ]);

            return $this->render('banque/index2.html.twig', array(
                'banques' => $banques,
                'applications' => $this->findApplications()

            ));
        }

        return $this->render('banque/index.html.twig', array(
            'banques' => $banques,
            'applications' => $this->findApplications()

        ));
    }

    /**
     * Creates a new banque entity.
     *
     * @Route("/new", name="banque_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $banque = $em->getRepository('AppBundle:Banque')->findOneBy([
            'utilisateur' => $this->getUser()
        ]);
        if($banque){
            $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Vous avez déjà enregistré vos informations bancaires.</h3>Mettez-les à jour ici.');
            return $this->redirectToRoute('banque_index');
        }

        $banque = new Banque();
        $banque->setUtilisateur($this->getUser());
        $form = $this->createForm('AppBundle\Form\BanqueType', $banque);
        $form->remove('utilisateur');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($banque);
            $em->flush();

            return $this->redirectToRoute('banque_index');
        }

        return $this->render('banque/new.html.twig', array(
            'banque' => $banque,
            'applications' => $this->findApplications(),
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a banque entity.
     *
     * @Route("/{id}", name="banque_show")
     * @Method("GET")
     */
    public function showAction(Banque $banque)
    {
        $deleteForm = $this->createDeleteForm($banque);

        return $this->render('banque/show.html.twig', array(
            'banque' => $banque,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing banque entity.
     *
     * @Route("/{id}/edit", name="banque_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Banque $banque)
    {
        $deleteForm = $this->createDeleteForm($banque);
        $editForm = $this->createForm('AppBundle\Form\BanqueType', $banque);
        $editForm->remove('utilisateur');
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', '<h3>Mise à jour des coordonnées effectuée avec succès</h3>Les nouvelles informations seront prises en compte lors des versements futurs.');
            return $this->redirectToRoute('banque_index');
        }

        return $this->render('banque/edit.html.twig', array(
            'banque' => $banque,
            'form' => $editForm->createView(),
            'applications' => $this->findApplications(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a banque entity.
     *
     * @Route("/{id}", name="banque_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Banque $banque)
    {
        $form = $this->createDeleteForm($banque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$em->remove($banque);
            $em->flush();
        }

        return $this->redirectToRoute('banque_index');
    }

    /**
     * Creates a form to delete a banque entity.
     *
     * @param Banque $banque The banque entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Banque $banque)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('banque_delete', array('id' => $banque->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
