<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Frais;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Frai controller.
 *
 * @Route("manage/frais")
 */
class FraisController extends Controller
{
    /**
     * Lists all frai entities.
     *
     * @Route("/", name="frais_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $frais = $em->getRepository('AppBundle:Frais')->findAll();

        return $this->render('frais/index.html.twig', array(
            'frais' => $frais,
        ));
    }

    /**
     * Creates a new frai entity.
     *
     * @Route("/new", name="frais_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $frai = new Frai();
        $form = $this->createForm('AppBundle\Form\FraisType', $frai);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$em->persist($frai);
            $em->flush();

            return $this->redirectToRoute('frais_show', array('id' => $frai->getId()));
        }

        return $this->render('frais/new.html.twig', array(
            'frai' => $frai,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a frai entity.
     *
     * @Route("/{id}", name="frais_show")
     * @Method("GET")
     */
    public function showAction(Frais $frai)
    {
        $deleteForm = $this->createDeleteForm($frai);

        return $this->render('frais/show.html.twig', array(
            'frai' => $frai,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing frai entity.
     *
     * @Route("/{id}/edit", name="frais_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Frais $frai)
    {
        $deleteForm = $this->createDeleteForm($frai);
        $editForm = $this->createForm('AppBundle\Form\FraisType', $frai);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('frais_edit', array('id' => $frai->getId()));
        }

        return $this->render('frais/edit.html.twig', array(
            'frai' => $frai,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a frai entity.
     *
     * @Route("/{id}", name="frais_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Frais $frai)
    {
        $form = $this->createDeleteForm($frai);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            //$em->remove($frai);
            $em->flush();
        }

        return $this->redirectToRoute('frais_index');
    }

    /**
     * Creates a form to delete a frai entity.
     *
     * @param Frais $frai The frai entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Frais $frai)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('frais_delete', array('id' => $frai->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
