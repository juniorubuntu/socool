<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\UtilisateurRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Utilisateur extends BaseUser {

    public function __toString() {
        return ($this->getNom() != '') ? $this->getNom() : 'Pas de Nom';
    }
    use Timestamps;
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $nom;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sexe;

    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $session;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cni;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pays;


    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    private $codePostal;


    /**
     * @var float
     *
     * @ORM\Column(name="fonds", type="float", nullable=true)
     */
    private $fonds;


    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $typeCompte;

    /**
     * @ORM\ManyToOne(targetEntity="Droit")
     * @Assert\NotBlank()
     */
    private $level;

    /**
     * @var date
     * @ORM\Column(type="date",  nullable=true)
     */
    private $date;

    /**
     * @var object
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application", mappedBy="application")
     * @ORM\JoinColumn(onDelete="CASCADE")
    */
    private $application;

    /**
     * @var object
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Banque", mappedBy="banque")
     * @ORM\JoinColumn(onDelete="CASCADE")
    */
    private $banque;

    /**
     *  @ORM\Column(name="github_id", type="string", length=255, nullable=true) 
     */
    protected $github_id;

    /**
     *  @ORM\Column(name="github_access_token", type="string", length=255, nullable=true) 
     */
    protected $github_access_token;

    /**
     *  @ORM\Column(name="facebook_id", type="string", length=255, nullable=true) 
     */
    protected $facebook_id;

    /**
     *  @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true) 
     */
    protected $facebook_access_token;

    /**
     *  @ORM\Column(name="googleplus_id", type="string", length=255, nullable=true) 
     */
    protected $googleplus_id;

    /**
     *  @ORM\Column(name="googleplus_access_token", type="string", length=255, nullable=true) 
     */
    protected $googleplus_access_token;

    /**
     *  @ORM\Column(name="stackexchange_id", type="string", length=255, nullable=true) 
     */
    protected $stackexchange_id;

    /**
     *  @ORM\Column(name="stackexchange_access_token", type="string", length=255, nullable=true) 
     */
    protected $stackexchange_access_token;

    public function setGithubId($githubId) {
        $this->github_id = $githubId;

        return $this;
    }

    public function getGithubId() {
        return $this->github_id;
    }

    public function setGithubAccessToken($githubAccessToken) {
        $this->github_access_token = $githubAccessToken;

        return $this;
    }

    public function getGithubAccessToken() {
        return $this->github_access_token;
    }

    public function setFacebookId($facebookID) {
        $this->facebook_id = $facebookID;

        return $this;
    }

    public function getFacebookId() {
        return $this->facebook_id;
    }

    public function setFacebookAccessToken($facebookAccessToken) {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    public function getFacebookAccessToken() {
        return $this->facebook_access_token;
    }

    public function setGoogleplusId($googlePlusId) {
        $this->googleplus_id = $googlePlusId;

        return $this;
    }

    public function getGoogleplusId() {
        return $this->googleplus_id;
    }

    public function setGoogleplusAccessToken($googleplusAccessToken) {
        $this->googleplus_access_token = $googleplusAccessToken;

        return $this;
    }

    public function getGoogleplusAccessToken() {
        return $this->googleplus_access_token;
    }

    public function setStackexchangeId($stackExchangeId) {
        $this->stackexchange_id = $stackExchangeId;

        return $this;
    }

    public function getStackexchangeId() {
        return $this->stackexchange_id;
    }

    public function setStackexchangeAccessToken($stackExchangeAccessToken) {
        $this->stackexchange_access_token = $stackExchangeAccessToken;

        return $this;
    }

    public function getStackexchangeAccessToken() {
        return $this->stackexchange_access_token;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Utilisateur
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return Utilisateur
     */
    public function setSexe($sexe) {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe() {
        return $this->sexe;
    }

    /**
     * Set cni
     *
     * @param string $cni
     *
     * @return Utilisateur
     */
    public function setCni($cni) {
        $this->cni = $cni;

        return $this;
    }

    /**
     * Get cni
     *
     * @return string
     */
    public function getCni() {
        return $this->cni;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return Utilisateur
     */
    public function setTelephone($telephone) {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone() {
        return $this->telephone;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return Utilisateur
     */
    public function setAdresse($adresse) {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse() {
        return $this->adresse;
    }

    /**
     * Set level
     *
     * @param \UserBundle\Entity\Droit $level
     *
     * @return Utilisateur
     */
    public function setLevel(\UserBundle\Entity\Droit $level = null) {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return \UserBundle\Entity\Droit
     */
    public function getLevel() {
        return $this->level;
    }


    /**
     * Set pays
     *
     * @param string $pays
     *
     * @return Utilisateur
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }


    /**
     * Set session
     *
     * @param string $session
     *
     * @return Utilisateur
     */
    public function setSession($session)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return string
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set fonds
     *
     * @param float $fonds
     *
     * @return Utilisateur
     */
    public function setFonds($fonds)
    {
        $this->fonds = $fonds;

        return $this;
    }

    /**
     * Get fonds
     *
     * @return float
     */
    public function getFonds()
    {
        return $this->fonds;
    }

    /**
     * Set codePostal
     *
     * @param integer $codePostal
     *
     * @return Utilisateur
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal
     *
     * @return integer
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Utilisateur
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set typeCompte
     *
     * @param string $typeCompte
     *
     * @return Utilisateur
     */
    public function setTypeCompte($typeCompte)
    {
        $this->typeCompte = $typeCompte;

        return $this;
    }

    /**
     * Get typeCompte
     *
     * @return string
     */
    public function getTypeCompte()
    {
        return $this->typeCompte;
    }

    /**
     * Add application
     *
     * @param \AppBundle\Entity\Application $application
     *
     * @return Utilisateur
     */
    public function addApplication(\AppBundle\Entity\Application $application)
    {
        $this->application[] = $application;

        return $this;
    }

    /**
     * Remove application
     *
     * @param \AppBundle\Entity\Application $application
     */
    public function removeApplication(\AppBundle\Entity\Application $application)
    {
        $this->application->removeElement($application);
    }

    /**
     * Get application
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * Set banque
     *
     * @param \AppBundle\Entity\Banque $banque
     *
     * @return Utilisateur
     */
    public function setBanque(\AppBundle\Entity\Banque $banque = null)
    {
        $this->banque = $banque;

        return $this;
    }

    /**
     * Get banque
     *
     * @return \AppBundle\Entity\Banque
     */
    public function getBanque()
    {
        return $this->banque;
    }
}
